import unittest

from spotlight.lcd import vm

class TestMemorySegment(unittest.TestCase):

	def test_address_in_segment(self):
		segment = vm.MemorySegment(0x08, 8)

		self.assertTrue(0x08 in segment)
		self.assertTrue(0x0f in segment)

		self.assertFalse(0x07 in segment)
		self.assertFalse(0x10 in segment)

	def test_length_of_segment(self):
		self.assertEqual(1, len(vm.MemorySegment(0x00, 1)))
		self.assertEqual(10, len(vm.MemorySegment(0x00, 10)))
		self.assertEqual(100, len(vm.MemorySegment(0x00, 100)))

	def test_zero_length_segment(self):
		with self.assertRaises(ValueError):
			vm.MemorySegment(0x00, 0)

	def test_negative_length_segment(self):
		with self.assertRaises(ValueError):
			vm.MemorySegment(0x00, -1)

	def test_negative_address_for_segment(self):
		with self.assertRaises(ValueError):
			vm.MemorySegment(-0x08, 8)

	def test_offset_of_address_in_segment(self):
		segment = vm.MemorySegment(0x80, 16)
		offsets = range(0x80, 0x80 + len(segment))
		for index, address in enumerate(offsets):
			self.assertEqual(address, segment.address_of(index))

	def test_address_of_offset_in_segment(self):
		segment = vm.MemorySegment(0x80, 16)
		offsets = range(0x80, 0x80 + len(segment))
		for index, address in enumerate(offsets):
			self.assertEqual(index, segment.offset_of(address))

	def test_invalid_offset_in_segment(self):
		segment = vm.MemorySegment(0x80, 16)
		with self.assertRaises(ValueError):
			segment.address_of(-1)
		with self.assertRaises(ValueError):
			segment.address_of(16)

	def test_invalid_address_in_segment(self):
		segment = vm.MemorySegment(0x80, 16)
		with self.assertRaises(ValueError):
			segment.offset_of(0x7f)
		with self.assertRaises(ValueError):
			segment.offset_of(0x80 + 16)

	def test_repr_for_segment(self):
		segment = vm.MemorySegment(0x100, 1024)
		repr_string = "MemoryRegion(address=0x%08x, length=%d)" % (0x100, 1024)
		self.assertEqual(repr_string, repr(segment))

	def test_iter_for_segment(self):
		segment = vm.MemorySegment(0x08, 8)
		actual_addresses = [addr for addr in segment]
		expected_addresses = list(range(0x08, 0x08 + 8))

		self.assertEqual(expected_addresses, actual_addresses)
