import unittest
import os

DIR = os.path.dirname(__file__)

CAPE_EEPROM_FIXTURE_PATH = os.path.join(DIR, 'fixtures', 'cape_eeprom')

from spotlight.beaglebone.cape import CapeIdentity


class TestCapeIdentity(unittest.TestCase):

    def setUp(self):
        with open(CAPE_EEPROM_FIXTURE_PATH) as handle:
            self.cape_eeprom_data = handle.read()

    def test_cape_identity(self):
        """
        Non-exhaustive test of verify() and important fields.
        """
        cape_identity = CapeIdentity.from_bytes(self.cape_eeprom_data)

        self.assertTrue(cape_identity.verify())

        self.assertEqual(cape_identity.board_name, "Petropower")
        self.assertEqual(cape_identity.version, "A")
        self.assertEqual(cape_identity.manufacturer, "Petropower")
        self.assertEqual(cape_identity.part_number, "1023")
        self.assertEqual(cape_identity.serial_number, "CC1502606")
