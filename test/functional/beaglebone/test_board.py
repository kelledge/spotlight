"""
NOTE: Copy-Paste-Ah! See ./test_cape.py
"""
import unittest
import os

DIR = os.path.dirname(__file__)

BOARD_EEPROM_FIXTURE_PATH = os.path.join(DIR, 'fixtures', 'board_eeprom')

from spotlight.beaglebone.board import BoardIdentity


class TestCapeIdentity(unittest.TestCase):

    def setUp(self):
        with open(BOARD_EEPROM_FIXTURE_PATH) as handle:
            self.board_eeprom_data = handle.read()

    def test_cape_identity(self):
        """
        Non-exhaustive test of verify() and important fields.
        """
        board_identity = BoardIdentity.from_bytes(self.board_eeprom_data)

        self.assertTrue(board_identity.verify())

        self.assertEqual(board_identity.name, "A335BNLT")
        self.assertEqual(board_identity.version, "000C")
        self.assertEqual(board_identity.serial, "2015BBBK0420")
        #self.assertEqual(board_identity.mac_addr, "1023")
