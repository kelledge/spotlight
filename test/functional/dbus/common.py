from txdbus import objects
from txdbus.interface import DBusInterface, Method

class SimpleDBusObject(objects.DBusObject):
    """
    Very simple DBus object for use in functional tests.
    """
    dbusInterfaces = [
        DBusInterface(
            'test.spotlight',
            Method('EchoString', arguments='s', returns='s' )
        )
    ]

    @objects.dbusMethod('test.spotlight', 'EchoString')
    def EchoString(self, string):
        return string
