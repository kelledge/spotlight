from twisted.trial import unittest
from twisted.internet import reactor, defer

from txdbus import client

from spotlight.dbus.bus import getSystemBus, getSessionBus, MessageBus

class TestGetSystemBus(unittest.TestCase):

    @defer.inlineCallbacks
    def test_system_bus_is_singleton(self):
        bus1 = yield getSystemBus(reactor)
        bus2 = yield getSystemBus(reactor)

        self.assertTrue(bus1 is bus2)

        yield bus1.disconnect()

    @defer.inlineCallbacks
    def test_session_bus_is_singleton(self):
        bus1 = yield getSessionBus(reactor)
        bus2 = yield getSessionBus(reactor)

        self.assertTrue(bus1 is bus2)

        yield bus1.disconnect()


class TestMessageBus(unittest.TestCase):

    def setUp(self):
        pass
        #print 'SETUP'
        # This fails for some reason
        #self.conn = yield getSessionBus(reactor)

        # This works for some reason
        #self.left_conn = yield client.connect(reactor, busAddress='session')
        #self.right_conn = yield client.connect(reactor, busAddress='session')
        #self.messageBus = yield MessageBus.fromConnection(self.left_conn)


    def tearDown(self):
        pass
        #print 'TEARDOWN'
        #self.messageBus.disconnect()
        #yield self.left_conn.disconnect()
        #yield self.right_conn.disconnect()

    @defer.inlineCallbacks
    def test_notify_name_acquire(self):
        leftConn = yield client.connect(reactor, busAddress='session')
        rightConn = yield client.connect(reactor, busAddress='session')

        yield leftConn.requestBusName('test.spotlight')
        yield leftConn.releaseBusName('test.spotlight')

        yield leftConn.disconnect()
        yield rightConn.disconnect()

        self.assertTrue(True)

    @defer.inlineCallbacks
    def test_notify_name_changed(self):
        leftConn = yield client.connect(reactor, busAddress='session')
        rightConn = yield client.connect(reactor, busAddress='session')

        leftUniqueName = leftConn.busName
        rightUniqueName = rightConn.busName

        messageBus = yield MessageBus.fromConnection(leftConn)

        yield leftConn.requestBusName('test.spotlight')
        yield rightConn.requestBusName(
                    'test.spotlight',
                    doNotQueue=False,
                    errbackUnlessAcquired=False
        )

        yield leftConn.releaseBusName('test.spotlight')
        yield rightConn.releaseBusName('test.spotlight')

        yield leftConn.disconnect()
        yield rightConn.disconnect()
