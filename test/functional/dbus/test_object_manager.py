# from twisted.internet import gireactor
# gireactor.install()


from twisted.trial import unittest

from twisted.internet import reactor, defer, task

from txdbus import client, objects, marshal
from txdbus.interface import DBusInterface, Method, Signal, Property

from spotlight.dbus.object_manager import LocalObjectManager, RemoteObjectManager
from spotlight.dbus.bus import getSessionBus

from pprint import pprint


class TestRemoteObjectManager(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    @defer.inlineCallbacks
    def test_remote_object_manager_emits_signals(self):
        conn = yield client.connect(reactor, busAddress='session')
        yield conn.requestBusName('test.spotlight')

        localMgr = LocalObjectManager(conn, '/Test')

        remoteMgr = yield RemoteObjectManager.fromObjectPath(
            conn,
            'test.spotlight',
            '/Test'
        )

        def printAdd(sender, **kwargs):
            print 'ADD', kwargs['objectPath']

        def printRemove(sender, **kwargs):
            print 'REM', kwargs['objectPath']

        remoteMgr.objectAdded.connect(printAdd)
        remoteMgr.objectRemoved.connect(printRemove)

        o1 = objects.DBusObject('/Test/1')
        o2 = objects.DBusObject('/Test/2')
        o3 = objects.DBusObject('/Test/3')

        localMgr.addObject(o1)
        localMgr.addObject(o2)
        localMgr.addObject(o3)

        yield task.deferLater(reactor, 0.1, lambda: None)
        pprint(remoteMgr.getManagedObjects())

        localMgr.removeObject(o3)
        localMgr.removeObject(o1)
        localMgr.removeObject(o2)

        yield task.deferLater(reactor, 0.1, lambda: None)
        pprint(remoteMgr.getManagedObjects())

        localMgr.addObject(o3)
        localMgr.addObject(o1)

        yield task.deferLater(reactor, 0.1, lambda: None)
        pprint(remoteMgr.getManagedObjects())

        localMgr.addObject(o2)

        yield task.deferLater(reactor, 0.1, lambda: None)
        pprint(remoteMgr.getManagedObjects())

        self.assertTrue(True)

        yield conn.releaseBusName('test.spotlight')
        yield conn.disconnect()

    @defer.inlineCallbacks
    def test_object_manager_already_has_managed_objects(self):
        conn = yield client.connect(reactor, busAddress='session')
        yield conn.requestBusName('test.spotlight1')

        localMgr = LocalObjectManager(conn, '/Test')

        o1 = objects.DBusObject('/Test/1')
        o2 = objects.DBusObject('/Test/2')
        o3 = objects.DBusObject('/Test/3')

        localMgr.addObject(o1)
        localMgr.addObject(o2)
        localMgr.addObject(o3)

        remoteMgr = yield RemoteObjectManager.fromObjectPath(
            conn,
            'test.spotlight1',
            '/Test'
        )

        pprint(remoteMgr.getManagedObjects())
        yield conn.releaseBusName('test.spotlight1')
        yield conn.disconnect()
