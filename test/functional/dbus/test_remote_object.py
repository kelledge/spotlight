from twisted.trial import unittest
from twisted.internet import reactor, defer
from twisted.internet import error as txError

from txdbus import client, objects, error
from txdbus.interface import DBusInterface, Method

from spotlight.dbus.bus import getSystemBus, getSessionBus


from .common import SimpleDBusObject


class TestRemoteObjectConnectionInterrupt(unittest.TestCase):
    """
    Test txdbus.objects.DBusRemoteObject behavior when local or remote
    connections are interrupted *after* being successfully exported by the
    remote and acquired by the local.

    AttributeError
    TypeError

    twisted.internet.error.ConnectionDone

    txdbus.error.MarshallingError
    txdbus.error.RemoteError
      org.freedesktop.DBus.Error.UnknownObject
      org.freedesktop.DBus.Error.NoReply
      org.freedesktop.DBus.Error.UnknownMethod
      org.freedesktop.DBus.Error.InvalidArgs

    txdbus.error.IntrospectionFailed
      org.freedesktop.DBus.Error.ServiceUnknown
    """
    @defer.inlineCallbacks
    def setUp(self):
        self.left_bus = yield client.connect(reactor, busAddress='session')
        self.right_bus = yield client.connect(reactor, busAddress='session')

        yield self.right_bus.requestBusName('test.spotlight')

        self.localObject = SimpleDBusObject('/Test/1')
        self.right_bus.exportObject(self.localObject)

        self.remoteObject = yield self.left_bus.getRemoteObject(
            'test.spotlight',
            '/Test/1'
        )

    @defer.inlineCallbacks
    def tearDown(self):
        yield self.left_bus.disconnect()
        yield self.right_bus.disconnect()

    @defer.inlineCallbacks
    def test_known_remote_method_call(self):
        """
        Attempt to call a method on an object proxy.
        """
        result = yield self.remoteObject.callRemote('EchoString', 'TEST')
        self.assertEqual('TEST', result)

    @defer.inlineCallbacks
    def test_unknown_remote_method_call(self):
        """
        Attempt to call an unknown method on an object proxy.
        """
        try:
            yield self.remoteObject.callRemote('NotAMethod', 'TEST')
        except AttributeError as e:
            # We expected `exceptions.AttributeError`
            pass
        except Exception as e:
            self.fail(e)
        else:
            self.fail()

    @defer.inlineCallbacks
    def test_incorrect_remote_method_argument_length(self):
        """
        Attempt to call a method on an object proxy with an incorrect number of
        arguments
        """
        try:
            yield self.remoteObject.callRemote('EchoString', 'TEST', 1, [3])
        except TypeError as e:
            # We expected `exceptions.TypeError`
            pass
        except Exception as e:
            self.fail(type(e))
        else:
            self.fail()

    @defer.inlineCallbacks
    def test_incorrect_remote_method_argument_type(self):
        """
        Attempt to call a method on an object proxy with the incorrect parameter
        type.
        """
        try:
            yield self.remoteObject.callRemote('EchoString', ['TEST'])
        except error.MarshallingError as e:
            # We expected `txdbus.error.MarshallingError`
            pass
        except Exception as e:
            self.fail(type(e))
        else:
            self.fail()

    @defer.inlineCallbacks
    def test_unknown_object(self):
        """
        Attempt to call a method on an object that has been unexported.

        We could use assertFailure here, but given that these tests are written
        as inlineCallbacks, I've opted for the synchronous-looking approach.
        """
        try:
            self.right_bus.unexportObject('/Test/1')
            yield self.remoteObject.callRemote('EchoString', 'TEST')
        except error.RemoteError as e:
            self.assertEqual(
                e.errName,
                "org.freedesktop.DBus.Error.UnknownObject"
            )
        except Exception as e:
            self.fail(e)
        else:
            self.fail()

    @defer.inlineCallbacks
    def test_no_reply(self):
        """
        Attempt to call a method on an object exported by a service that is no
        longer connected to the bus on its well-known name.
        """
        try:
            yield self.right_bus.disconnect()
            yield self.remoteObject.callRemote('EchoString', 'TEST')
        except error.RemoteError as e:
            self.assertEqual(
                e.errName,
                "org.freedesktop.DBus.Error.NoReply"
            )
        except Exception as e:
            self.fail(e)
        else:
            self.fail()


    @defer.inlineCallbacks
    def test_left_disconnected(self):
        """
        Attempt to call a method on an object proxy after losing the local DBus
        connection.
        """
        try:
            yield self.left_bus.disconnect()
            yield self.remoteObject.callRemote('EchoString', 'TEST')
        except txError.ConnectionDone as e:
            # We expected `twisted.internet.error.ConnectionDone`
            pass
        except Exception as e:
            self.fail(e)
        else:
            self.fail()
