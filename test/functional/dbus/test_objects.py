from twisted.trial import unittest
from twisted.internet import reactor, defer

from txdbus import client, objects, error
from txdbus.interface import DBusInterface, Method

from spotlight.dbus.bus import getSystemBus, getSessionBus
from spotlight.dbus.objects import ManagedRemoteDBusObject


from .common import SimpleDBusObject


class TestManagedRemoteDBusObject(unittest.TestCase):
    """

    """
    @defer.inlineCallbacks
    def setUp(self):
        self.left_bus = yield client.connect(reactor, busAddress='session')
        self.right_bus = yield client.connect(reactor, busAddress='session')

        yield self.right_bus.requestBusName('test.spotlight')

        self.localObject = SimpleDBusObject('/Test/1')
        self.right_bus.exportObject(self.localObject)

        self.remoteObject = yield self.left_bus.getRemoteObject(
            'test.spotlight',
            '/Test/1'
        )
        self.managedRemoteObject = ManagedRemoteDBusObject(self.remoteObject)

    @defer.inlineCallbacks
    def tearDown(self):
        yield self.left_bus.disconnect()
        yield self.right_bus.disconnect()

    def test_delegated_attribute(self):
        busName = self.managedRemoteObject.busName
        self.assertEqual(busName, 'test.spotlight')

    @defer.inlineCallbacks
    def test_delegated_method(self):
        result = yield self.managedRemoteObject.callRemote('EchoString', 'TEST')
        self.assertEqual(result, 'TEST')
