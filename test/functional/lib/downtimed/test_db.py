import unittest
import datetime
import os

DIR = os.path.dirname(__file__)

FIXTURE_PATH = os.path.join(DIR, 'fixtures', 'downtimedb')

from spotlight.lib.downtimed.db import Database

class TestDatabase(unittest.TestCase):
    def test_database_load(self):
        data = open(FIXTURE_PATH)
        db = Database(data)
        self.assertEqual(len(db), 28)
        data.close()

    def test_database_select(self):
        data = open(FIXTURE_PATH)
        db = Database(data)
        records = db.select(
            datetime.datetime(2016, 8, 25),
            datetime.datetime(2016, 9, 3)
        )
        print list(records)
        self.assertEqual(len(db), 28)
        data.close()

    def test_times(self):
        data = open(FIXTURE_PATH)
        db = Database(data)
        from pprint import pprint
        pprint(db.times())
        data.close()
