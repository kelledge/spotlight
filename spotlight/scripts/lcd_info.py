import pyudev

from spotlight.hardware import Hardware
from spotlight.lcd.lcd import LCDSysfs


template = """ \
LCD Information:
  Dimension:       {rows}x{columns}
  Cursor:          {cursor}\
"""

def main():
    ctx = pyudev.Context()
    hw = Hardware(ctx)
    lcd = LCDSysfs(hw.cape_lcd)

    rows, columns = lcd.dimension
    cursor = lcd.cursor

    print template.format(rows=rows,
                          columns=columns,
                          cursor=cursor)


if __name__ == '__main__':
    main()
