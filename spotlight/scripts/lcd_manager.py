import sys
import pyudev
import itertools
import time

from spotlight.hardware import Hardware
from spotlight.lcd.lcd import LCDSysfs, Lcd
from spotlight.lcd.apps import CellularState, Uptime
from spotlight.lib.mm.manager import get_modem_manager_object, get_system_bus, Manager

def main():
    ctx = pyudev.Context()
    hw = Hardware(ctx)
    lcd_sysfs = LCDSysfs(hw.cape_lcd)
    lcd = Lcd(lcd_sysfs)

    mm = get_modem_manager_object()
    bus = get_system_bus()
    m = Manager(bus, mm)
    modem = m.list_modems()[0]

    cs = CellularState(lcd, modem)
    cs.start()

    u = Uptime(lcd)
    u.run()

if __name__ == '__main__':
    main()
