import sys
import pyudev
import itertools
import time

from spotlight.hardware import Hardware
from spotlight.lcd.lcd import LCDSysfs, Lcd


def main():
    ctx = pyudev.Context()
    hw = Hardware(ctx)
    lcd_sysfs = LCDSysfs(hw.cape_lcd)
    lcd = Lcd(lcd_sysfs)

    lcd.ddram.write('abcdefgh')
    lcd.ddram.write('ijklmnop')


if __name__ == '__main__':
    main()
