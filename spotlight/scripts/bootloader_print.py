import pyudev
import ctypes
import os

from spotlight.hardware import Hardware
from spotlight.beaglebone.bootloader import OMAPDiskInspector, SPLImage, UBootImage


template = """\
Cape Identity:
  Name:          {}
  Version:       {}
  Manufacturer:  {}
  Part Number:   {}
  Serial Number: {}\
"""


def main():
    c = pyudev.Context()
    h = Hardware(c)

    inspector = OMAPDiskInspector(h.board_nand['DEVNAME'], [SPLImage, UBootImage])
    images = inspector.inspect()
    for image in images:
        image.print_image()
        print ''

if __name__ == '__main__':
    main()
