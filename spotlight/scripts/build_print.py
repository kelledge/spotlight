"""
This should likely become its own module so other tools can more easily access
build meta information. For now, it is enough that this script exists. It should
serve as a reminder that this information is valuable and should get its own
module soon.

It should also implement some simple shell variable parser.
"""

BUILD_INFO_PATH='/etc/.build_info'

def main():
    with open(BUILD_INFO_PATH) as handle:
        build_info = handle.read()

    print build_info

if __name__ == '__main__':
    main()
