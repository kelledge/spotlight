from twisted.internet import defer

from spotlight.dbus.bus import getSystemBus
from spotlight.lib.mm.manager import ModemManager

from pprint import pprint

@defer.inlineCallbacks
def main(reactor):
    def printModems(sender, **kwargs):
        modem = kwargs.get("modem")
        pprint(sender.getManagedModems())

    connection = yield getSystemBus(reactor)
    modemManager = yield ModemManager.fromConnection(connection)
    yield modemManager.connect()
    pprint(modemManager.getManagedModems())

    modemManager.modemAdded.connect(printModems)
    modemManager.modemRemoved.connect(printModems)

if __name__ == '__main__':
    from twisted.internet import reactor
    reactor.callWhenRunning(main, reactor)
    reactor.run()
