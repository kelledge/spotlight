from twisted.internet import defer, task

from spotlight.lib.mm.manager import getModemManager


@defer.inlineCallbacks
def main(reactor):
    modemManager = yield getModemManager()
    yield modemManager.connect()


if __name__ == '__main__':
    task.react(main)
