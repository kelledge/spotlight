import pyudev
import ctypes
import os

from spotlight.hardware import Hardware
from spotlight.beaglebone.cape import CapeIdentity


template = """\
Cape Identity:
  Name:          {}
  Version:       {}
  Manufacturer:  {}
  Part Number:   {}
  Serial Number: {}\
"""


def main():
    c = pyudev.Context()
    h = Hardware(c)

    with open(os.path.join(h.cape_eeprom.sys_path, 'eeprom')) as handle:
        # This load is pretty nasty. Perhaps a from_file method would be useful.
        # It should do the sizeof handling in the class method
        cape_identity = CapeIdentity.from_bytes(handle.read(ctypes.sizeof(CapeIdentity)))

    if cape_identity.verify():
        print template.format(
             cape_identity.board_name,
             cape_identity.version,
             cape_identity.manufacturer,
             cape_identity.part_number,
             cape_identity.serial_number
        )
    else:
        print "Could not decode cape identity!"

if __name__ == '__main__':
    main()
