from twisted.internet import defer

from spotlight.dbus.bus import getSystemBus, MessageBus


@defer.inlineCallbacks
def main(reactor):
    connection = yield getSystemBus(reactor)
    messageBus = yield MessageBus.fromConnection(connection)
    print messageBus

if __name__ == '__main__':
    from twisted.internet import reactor
    reactor.callWhenRunning(main, reactor)
    reactor.run()
