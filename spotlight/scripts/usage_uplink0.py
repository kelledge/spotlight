import os
from spotlight.lib.vnstat.db import Data, DEFAULT_DATABASE_DIRECTORY


def main():
    db_path = os.path.join(DEFAULT_DATABASE_DIRECTORY, 'uplink0')

    with open(db_path) as handle:
        db = Data.from_bytes(handle.read())

    print "Interface: %s" % db.interface
    print "Version:   %s" % db.version
    print "Created:   %s" % db.created
    print "Updated:   %s" % db.lastupdated

    for d in db.day:
        print d

    for h in db.hour:
        print h

if __name__ == '__main__':
    main()
