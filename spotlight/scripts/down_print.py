import os
from spotlight.lib.downtimed.db import Database, DEFAULT_DATABASE_DIRECTORY


def main():
    db_path = os.path.join(DEFAULT_DATABASE_DIRECTORY, 'downtimedb')

    with open(db_path) as handle:
        db = Database(handle)

    for r in db:
        print r

if __name__ == '__main__':
    main()
