import pyudev
import ctypes
import os

from spotlight.hardware import Hardware
from spotlight.beaglebone.board import BoardIdentity


template = """\
Beaglebone Identity:
  Name:    {}
  Version: {}
  Serial:  {}
  Config:  {!r}
  MAC:     {!r}\
"""


def main():
    c = pyudev.Context()
    h = Hardware(c)

    with open(os.path.join(h.board_eeprom.sys_path, 'eeprom')) as handle:
        # This load is pretty nasty. Perhaps a from_file method would be useful.
        # It should do the sizeof handling in the class method
        board_identity = BoardIdentity.from_bytes(handle.read(ctypes.sizeof(BoardIdentity)))

    if board_identity.verify():
        print template.format(
             board_identity.name,
             board_identity.version,
             board_identity.serial,
             board_identity.config,
             board_identity.mac_addr
        )
    else:
        print "Could not decode board identity!"

if __name__ == '__main__':
    main()
