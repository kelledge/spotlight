from pyee import EventEmitter
import dispatch

from twisted.internet import defer

from txdbus import client, objects, marshal
from txdbus.interface import DBusInterface, Method, Signal, Property


class LocalObjectManager(objects.DBusObject):
    """
    A Local DBus object implementing the 'org.freedesktop.DBus.ObjectManager'
    interface.

    Much of this functionality is copied from txdbus.objects.DBusObjectHandler.

    This is an odd class. I need to catalog the reasons why this class and
    txdbus.objects.DBusObjectHandler do not feel right. I believe this is a core
    architecture smell in txdbus.

    https://dbus.freedesktop.org/doc/dbus-specification.html#standard-interfaces-objectmanager
    """
    iface = DBusInterface(
        'org.freedesktop.DBus.ObjectManager',
        Method('GetManagedObjects', returns='a{oa{sa{sv}}}'),
        Signal('InterfacesAdded', 'oa{sa{sv}}'),
        Signal('InterfacesRemoved', 'oas')
    )

    dbusInterfaces = [iface]

    def __init__(self, conn, path):
        super(LocalObjectManager, self).__init__(path)

        self.conn = conn
        self.path = path

        self.managedObjects = {}

        self.conn.exportObject(self)

    def addObject(self, dbusObject):
        o = objects.IDBusObject( dbusObject )
        self.managedObjects[ o.getObjectPath() ] = o
        #o.setObjectHandler(self)

        i = dict()
        for iface in o.getInterfaces():
            i[ iface.name ] = o.getAllProperties( iface.name )

        self.conn.exportObject(dbusObject)
        self.emitSignal('InterfacesAdded', o.getObjectPath(), i)

    def removeObject(self, dbusObject):
        o = objects.IDBusObject( dbusObject )
        i = [ iface.name for iface in o.getInterfaces() ]
        self.conn.unexportObject(dbusObject.getObjectPath())
        self.emitSignal('InterfacesRemoved', o.getObjectPath(), i)

    @objects.dbusMethod('org.freedesktop.DBus.ObjectManager', 'GetManagedObjects')
    def getManagedObjects(self):
        """
        Returns a Python dictionary containing the reply content for
        org.freedesktop.DBus.ObjectManager.GetManagedObjects
        """
        d = dict()

        for p in sorted(self.managed_objects.keys()):
            o = self.managed_objects[p]
            i = dict()
            d[ p ] = i
            for iface in o.getInterfaces():
                i[ iface.name ] = o.getAllProperties( iface.name )

        return d


class RemoteObjectManager(object):
    """
    Emits the following events:
     1) 'add'
     2) 'remove'

    https://dbus.freedesktop.org/doc/dbus-specification.html#standard-interfaces-objectmanager
    """
    def __init__(self, connection, dbusObject):
        """
        :param dbusObject: The remote object proxy to the object manager
        :type dbusObject {DBusObject}
        """
        self.connection = connection
        self.dbusObject = dbusObject
        self.busName = dbusObject.busName

        self.managedObjectCache = {}
        self.cacheLock = defer.DeferredLock()

        self.signalAddedId = None
        self.signalRemovedId = None

        self.objectAdded = dispatch.Signal()
        self.objectRemoved = dispatch.Signal()

    @defer.inlineCallbacks
    def connect(self):
        """
        Connects all signals and fetches the list of objects currently being
        managed by the remote.

        :return: A deferred that will fire once connected to the remote object
                 manager
        :rtype: {defer.Deferred}

        WARN:
        These signals are never released (to my knowledge). I believe their
        DBus matches are persisted on the bus --perhaps even after the entire
        client disconnects. This is a major issue: The signals may not be
        disconnected and leak.
        """
        yield self._initializeCache()

        self.signalAddedId = yield self.dbusObject.notifyOnSignal(
            'InterfacesAdded',
            self.onInterfacesAdded,
            interface='org.freedesktop.DBus.ObjectManager'
        )
        self.signalRemovedId = yield self.dbusObject.notifyOnSignal(
            'InterfacesRemoved',
            self.onInterfacesRemoved,
            interface='org.freedesktop.DBus.ObjectManager'
        )

    @defer.inlineCallbacks
    def _initializeCache(self):
        """

        """
        print "INIT", self
        managedObjects = yield self.dbusObject.callRemote('GetManagedObjects')

        # Fan-out
        remoteObjects = []
        for objectPath, interfaceDefs in managedObjects.iteritems():
            d = self.connection.getRemoteObject(self.busName, objectPath)
            remoteObjects.append(d)

        # Wait for everything to be fetched
        dbusProxies = yield defer.DeferredList(remoteObjects)

        # Fan-in
        for success, proxy in dbusProxies:
            if success:
                self.managedObjectCache[proxy.objectPath] = proxy
            else:
                print 'WARN: Failed to create proxy'


    def disconnect(self):
        """
        Disconnects all signals.

        WARN:
        This method is not wired to be called.
        """
        self.dbusObject.cancelSignalNotification(self.signalAddedId)
        self.dbusObject.cancelSignalNotification(self.signalRemovedId)

    def getManagedObjects(self):
        """
        Return the list of currently managed objects.

        :return The dictionary of currently managed objects
        :rtype dict

        WARN: This returns a reference to the internal cache of managed objects.
        A misbehavied client could easily create a scenario where the object
        manager's cache and state is inconsistent with the real object manager.
        """
        return self.managedObjectCache

    @defer.inlineCallbacks
    def onInterfacesAdded(self, objectPath, interfaceDefs):
        """
        Handler for the 'InterfacesAdded' DBus signal

        WARN: This implementation does not handle objects that have gained an
        interface. It simply addes or overwrites the object in the list of
        managed objects as if that object had been created.

        It should probably merge the interface definition rather than
        overwriting it.
        """
        try:
            yield self.cacheLock.acquire()
            objectProxy = yield self.connection.getRemoteObject(
               self.busName,
               objectPath
            )
            self.managedObjectCache[objectPath] = objectProxy
            print objectPath, objectProxy
            self.objectAdded.send(
                sender=self,
                objectPath=objectPath,
                objectProxy=objectProxy
            )
        finally:
            self.cacheLock.release()

    @defer.inlineCallbacks
    def onInterfacesRemoved(self, objectPath, interfaceNames):
        """
        Handler for the 'InterfacesRemoved' DBus signal

        WARN: This implementation does not handle objects that have lost only a
        single interface. It simply removes the object from the list of
        objects manged by this mangager as if it were destroyed.

        It should check to see if the object still has interfaces before
        removing the object from this manager.
        """
        try:
            yield self.cacheLock.acquire()
            objectProxy = self.managedObjectCache.pop(objectPath)
            print objectPath, objectProxy
            self.objectRemoved.send(
                sender=self,
                objectPath=objectPath,
                objectProxy=objectProxy
            )
        except KeyError as error:
            # We lost track of an object somehow.
            print 'WARN: Inconsistent state!', error.message
        finally:
            self.cacheLock.release()


    @classmethod
    @defer.inlineCallbacks
    def fromObjectPath(cls, connection, busName, objectPath):
        """
        Instance creation helper.

        Creates the instance with the specified remote DBus object proxy and
        automatically calls the connect() method.

        :returns A RemoteObjectManager ready to use.
        """
        remoteObject = yield connection.getRemoteObject(
            busName,
            objectPath
        )
        obj = cls(connection, remoteObject)
        yield obj.connect()
        defer.returnValue(obj)
