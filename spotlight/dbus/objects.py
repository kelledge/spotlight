from dispatch import Signal

class ManagedRemoteDBusObject(object):
    """
    Composes txdbus.objects.RemoteDBusObject and adds additional functionality.

    This class can not simply subclass txdbus.objects.RemoteDBusObject because
    there is no simple way to get in the way of txdbus's creation of these
    objects.

    Mimics
    """
    def __init__(self, dbusProxy):
        self.dbusProxy = dbusProxy

    def proxyLost(self, reason):
        """
        Called when the remote proxy is lost.

        This could be because the object path is no longer exported or because
        the service exporting the proxy has released its name on the bus
        """

    def interfaceLost(self, interfaceName):
        """
        Called when this object loses an interface.
        """

    def interfaceAdded(self, interfaceName):
        """
        Called when this object gains an interface.
        """


    def __getattr__(self, attr):
        """
        Automatically delegate through to the wrapped DBus proxy object.

        https://docs.python.org/2/reference/datamodel.html#object.__getattr__

        Should only be called for attribute names not found on this object. Be
        careful of names
        """
        return getattr(self.dbusProxy, attr)
