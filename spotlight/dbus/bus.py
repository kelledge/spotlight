from twisted.internet import defer
from txdbus import client

from dispatch import Signal

SYSTEM_BUS = None
SESSION_BUS = None


MESSAGE_BUS_NAME = 'org.freedesktop.DBus'
MESSAGE_BUS_PATH = '/org/freedesktop/DBus'
MESSAGE_BUS_IFACE = 'org.freedesktop.DBus'


@defer.inlineCallbacks
def getSystemBus(reactor):
    """
    Get the system bus, or return the existing system bus.

    Does not handle errbacks.

    WARN:
    There is something wrong with these helpers. Their use in tests causes the
    reactor to hang. I am not sure why yet ... Oddly, they work just fine from
    scripts.
    """
    global SYSTEM_BUS

    if SYSTEM_BUS is None:
        bus = yield client.connect(reactor, busAddress='system')
        SYSTEM_BUS = bus
        defer.returnValue(bus)
    else:
        defer.returnValue(SYSTEM_BUS)


# NOTE: Copy-Paste-Ah!
@defer.inlineCallbacks
def getSessionBus(reactor):
    """
    Get the session bus, or return the existing session bus.

    Does not handle errbacks.

    WARN:
    There is something wrong with these helpers. Their use in tests causes the
    reactor to hang. I am not sure why yet ... Oddly, they work just fine from
    scripts.
    """
    global SESSION_BUS

    if SESSION_BUS is None:
        bus = yield client.connect(reactor, busAddress='session')
        SESSION_BUS = bus
        defer.returnValue(bus)
    else:
        defer.returnValue(SESSION_BUS)


def getMessageBusObject(systemBus):
    return systemBus.getRemoteObject(
        MESSAGE_BUS_NAME,
        MESSAGE_BUS_PATH
    )

@defer.inlineCallbacks
def getMessageBus(_reactor=None):
    from twisted.internet import reactor

    systemBus = yield getSystemBus(reactor)
    messageBusObject = yield getMessageBusObject(systemBus)
    messageBus = MessageBus(messageBusObject)

    defer.returnValue(messageBus)


class BusNameMonitor(object):
    """
    Monitors a well-known name for changes.
    """
    def __init__(self, messageBus, busName):
        """
        :param messageBus: An instance MessageBus.
        :param busName: The well-known bus name to monitor.
        """
        self.busName = busName
        self.messageBus = messageBus

        self.nameAcquired = Signal(providing_args=["newOwner"])
        self.nameReleased = Signal(providing_args=["oldOwner"])
        self.nameUpdated = Signal(providing_args=["newOwner", "oldOwner"])

    def connect(self):
        self.messageBus.nameAcquired.connect(self.onAcquired)
        self.messageBus.nameReleased.connect(self.onReleased)
        self.messageBus.nameUpdated.connect(self.onUpdated)

    def onAcquired(self, sender, **kwargs):
        name = kwargs.get("name")
        newOwner = kwargs.get("newOwner")

        if name == self.busName:
            self.nameAcquired.send(sender=self, newOwner=newOwner)

    def onReleased(self, sender, **kwargs):
        name = kwargs.get("name")
        oldOwner = kwargs.get("oldOwner")

        if name == self.busName:
            self.nameReleased.send(sender=self, oldOwner=oldOwner)

    def onUpdated(self, sender, **kwargs):
        name = kwargs.get("name")
        oldOwner = kwargs.get("oldOwner")
        newOwner = kwargs.get("newOwner")

        if name == self.busName:
            self.nameUpdated.send(sender=self, newOwner=newOwner, oldOwner=oldOwner)


class MessageBus(object):
    """
    org.freedesktop.DBus Interface - Message bus daemon interface

    Methods:
     *Lots*

    Signals:
    NameOwnerChanged (String  name, String old_owner, String new_owner)
    NameLost         (String  name)
    NameAcquired     (String  name)
    """
    def __init__(self, dbusObject):
        self.dbusObject = dbusObject

        self.nameOwnerChangedId = None

        self.nameAcquired = Signal(providing_args=["busName", "newOwner"])
        self.nameReleased = Signal(providing_args=["busName", "oldOwner"])
        self.nameUpdated = Signal(providing_args=["busName", "newOwner", "oldOwner"])

    @defer.inlineCallbacks
    def connect(self):
        self.nameOwnerChangedId = yield self.dbusObject.notifyOnSignal(
            'NameOwnerChanged',
            self.onNameOwnerChanged,
            interface='org.freedesktop.DBus'
        )

    def disconnect(self):
        self.dbusObject.cancelSignalNotification(self.nameOwnerChangedId)

    def monitorBusName(self, busName, onAcquired, onReleased, onUpdated):
        pass

    def onNameOwnerChanged(self, name, oldOwner, newOwner):
        """
        NameOwnerChanged (String  name, String  old_owner, String  new_owner)
        """
        if not name.startswith(":"): # Client has requested well-known name
            if oldOwner == "" and newOwner != "": # Acquired
                self.nameAcquired.send(
                    sender=self,
                    name=name,
                    newOwner=newOwner
                )
            elif oldOwner != "" and newOwner == "": # Release
                self.nameReleased.send(
                    sender=self,
                    name=name,
                    oldOwner=oldOwner
                )
            else: # Name owner updated to new unique ID
                self.nameUpdated.send(
                    sender=self,
                    name=name,
                    oldOwner=oldOwner,
                    newOwner=newOwner
                )
        else: # Client joined bus, but has not claimed a name
            pass

    @classmethod
    @defer.inlineCallbacks
    def fromConnection(cls, connection):
        remoteObject = yield connection.getRemoteObject(
            MESSAGE_BUS_NAME,
            MESSAGE_BUS_PATH
        )
        obj = cls(remoteObject)
        yield obj.connect()
        defer.returnValue(obj)
