"""
This module is intended to encapsulate the discovery and enumeration of device
hardware. This layer of abstraction allows the rest of this project to remain
platform independent as is reasonably possible and helps insulate {} from the
high-frequency changes in the ARM Linux

It should also be simple enough to stub out during testing.

All hardware devices are returned as {pyudev.Device} instances.
"""

import pyudev

class Hardware(object):

    def __init__(self, context):
        self.context = context

    @property
    def i2c_0(self):
        device = pyudev.Device.from_path(
            self.context,
            '/devices/platform/ocp/44e0b000.i2c/i2c-0'
        )
        return device

    @property
    def i2c_2(self):
        device = pyudev.Device.from_path(
            self.context,
            '/devices/platform/ocp/4819c000.i2c/i2c-2'
        )
        return device

    @property
    def cape_eeprom(self):
        e = pyudev.Enumerator(self.context).match_parent(self.i2c_2) \
                                           .match_subsystem('i2c') \
                                           .match_attribute('name', '24c256')
        for d in e:
            return d

    @property
    def cape_rtc(self):
        e = pyudev.Enumerator(self.context).match_parent(self.i2c_2) \
                                           .match_subsystem('i2c') \
                                           .match_attribute('name', 'ds1307')
        for d in e:
            return d

    @property
    def cape_lcd(self):
        device = pyudev.Device.from_path(
            self.context,
            '/devices/platform/lcd'
        )
        return device

    @property
    def board_leds(self):
        e = pyudev.Enumerator(self.context).match_subsystem('leds')
        return list(e)

    @property
    def board_gpios(self):
        e = pyudev.Enumerator(self.context).match_subsystem('gpio')
        return list(e)

    @property
    def board_eeprom(self):
        e = pyudev.Enumerator(self.context).match_parent(self.i2c_0) \
                                           .match_subsystem('i2c') \
                                           .match_attribute('name', '24c256')
        for d in e:
            return d

    @property
    def board_nand(self):
        """


        !WARN! This property simply returns the first match found. A more robust
        search should be done to ensure this is matched to the correct hardware.
        """
        device = pyudev.Device.from_path(
            self.context,
            '/sys/devices/platform/ocp/481d8000.mmc/mmc_host/mmc1'
        )
        e = pyudev.Enumerator(self.context).match_parent(device) \
                                           .match_subsystem('block')
        matches = list(e)
        return matches[0]

    @property
    def board_nand_boot0(self):
        e = pyudev.Enumerator(self.context).match_parent(self.board_nand) \
                                           .match_subsystem('block')
        for d in e:
            if 'boot0' in d.sys_path:
                return d

    @property
    def board_nand_boot1(self):
        e = pyudev.Enumerator(self.context).match_parent(self.board_nand) \
                                           .match_subsystem('block')
        for d in e:
            if 'boot1' in d.sys_path:
                return d


if __name__ == '__main__':
    c = pyudev.Context()
    h = Hardware(c)
    print h.i2c_0
    print h.i2c_2
    print h.cape_eeprom
    print h.cape_rtc
    print list(h.cape_lcd.attributes)
    print h.board_leds
    print h.board_gpios
    print h.board_eeprom
    print h.board_nand
    print h.board_nand_boot0
    print h.board_nand_boot1

    print h.board_nand['DEVNAME']
    print h.board_nand_boot0['DEVNAME']
    print h.board_nand_boot1['DEVNAME']
