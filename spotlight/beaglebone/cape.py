"""
Tools for working with BeagleBone capes.
"""
import ctypes

from spotlight.utils import AccessBytesMixin

EEPROM_HEADER = 0xAA5533EE
EEPROM_REVISION = 'A1'


class CapePinUsage(ctypes.BigEndianStructure, AccessBytesMixin):
    """
    Decodes and represents the contents of a cape's pin usage field.

    The definition for the fields here can be found in section 8.2.4,4 table 14
    of the BeagleBone Black System Reference Manual.
    """
    _fields_ = [
        ("used", ctypes.c_uint16, 1),
        ("direction", ctypes.c_uint16, 2),
        ("reserved", ctypes.c_uint16, 6),
        ("slew_rate", ctypes.c_uint16, 1),
        ("rx_enable", ctypes.c_uint16, 1),
        ("pullupdn", ctypes.c_uint16, 1),
        ("pullupdn_enable", ctypes.c_uint16, 1),
        ("mode", ctypes.c_uint16, 3),
    ]


class CapeIdentity(ctypes.BigEndianStructure, AccessBytesMixin):
    """
    Decodes and represents the contents of a cape's EEPROM configuration.

    The definition for the fields here can be found in section 8.2.4,4 table 14
    of the BeagleBone Black System Reference Manual.
    """
    _fields_ = [
        ("header", ctypes.c_uint32),
        ("eeprom_revision", ctypes.c_char * 2),
        ("board_name", ctypes.c_char * 32),
        ("version", ctypes.c_char * 4),
        ("manufacturer", ctypes.c_char * 16),
        ("part_number", ctypes.c_char * 16),
        ("pin_number", ctypes.c_uint16),
        ("serial_number", ctypes.c_char * 12),
        ("pin_usage", CapePinUsage * 74),
        ("vdd_3v3_current ", ctypes.c_uint16),
        ("vdd_3v3_current ", ctypes.c_uint16),
        ("sys_5v_current", ctypes.c_uint16),
        ("dc_supplied", ctypes.c_uint16),
    ]

    def verify(self):
        if self.header != EEPROM_HEADER:
            return False
        else:
            return True
