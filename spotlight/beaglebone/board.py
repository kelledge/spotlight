"""
Tools for working with BeagleBone boards.
"""
import ctypes

from spotlight.utils import AccessBytesMixin

#
# TI AM335x parts define a system EEPROM that defines certain sub-fields.
# We use these fields to in turn see what board we are on, and what
# that might require us to set or not set.
#
HDR_NO_OF_MAC_ADDR     = 3
HDR_ETH_ALEN           = 6
HDR_NAME_LEN           = 8
HDR_MAGIC     = 0xEE3355AA

BONEBLACK     = "A335BNLT"
BONEWHITE     = "A335BONE"

class BoardIdentity(ctypes.LittleEndianStructure, AccessBytesMixin):
    """
    struct am335x_baseboard_id {
        unsigned int magic;
        char name[HDR_NAME_LEN];
        char version[4];
        char serial[12];
        char config[32];
        char mac_addr[HDR_NO_OF_MAC_ADDR][HDR_ETH_ALEN];
    };
    """
    _fields_ = [
        ("magic", ctypes.c_uint32),
        ("name", ctypes.c_char * HDR_NAME_LEN),
        ("version", ctypes.c_char * 4),
        ("serial", ctypes.c_char * 12),
        ("config", ctypes.c_char * 32),
        ("mac_addr", ctypes.c_uint8 * HDR_NO_OF_MAC_ADDR * HDR_ETH_ALEN),
    ]

    def verify(self):
        if self.magic != HDR_MAGIC:
            return False
        else:
            return True
