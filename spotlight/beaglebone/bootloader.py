"""
Tools for working with the BeagleBone bootloader. Das U-Boot.
"""

import ctypes
import binascii
import itertools
import hashlib
import StringIO

from spotlight.utils import (
    AccessBytesMixin,
    chunk_file_reader
)

#
# Operating System Codes
#
IH_OS_INVALID          = 0       # Invalid OS
IH_OS_OPENBSD          = 1       # OpenBSD
IH_OS_NETBSD           = 2       # NetBSD
IH_OS_FREEBSD          = 3       # FreeBSD
IH_OS_4_4BSD           = 4       # 4.4BSD
IH_OS_LINUX            = 5       # Linux
IH_OS_SVR4             = 6       # SVR4
IH_OS_ESIX             = 7       # Esix
IH_OS_SOLARIS          = 8       # Solaris
IH_OS_IRIX             = 9       # Irix
IH_OS_SCO              = 10      # SCO
IH_OS_DELL             = 11      # Dell
IH_OS_NCR              = 12      # NCR
IH_OS_LYNXOS           = 13      # LynxOS
IH_OS_VXWORKS          = 14      # VxWorks
IH_OS_PSOS             = 15      # pSOS
IH_OS_QNX              = 16      # QNX
IH_OS_U_BOOT           = 17      # Firmware
IH_OS_RTEMS            = 18      # RTEMS
IH_OS_ARTOS            = 19      # ARTOS
IH_OS_UNITY            = 20      # Unity OS
IH_OS_INTEGRITY        = 21      # INTEGRITY
IH_OS_OSE              = 22      # OSE
IH_OS_PLAN9            = 23      # Plan 9
IH_OS_OPENRTOS         = 24      # OpenRTOS

#
# CPU Architecture Codes (supported by Linux)
#
IH_ARCH_INVALID        = 0       # Invalid CPU
IH_ARCH_ALPHA          = 1       # Alpha
IH_ARCH_ARM            = 2       # ARM
IH_ARCH_I386           = 3       # Intel x86
IH_ARCH_IA64           = 4       # IA64
IH_ARCH_MIPS           = 5       # MIPS
IH_ARCH_MIPS64         = 6       # MIPS  64 Bit
IH_ARCH_PPC            = 7       # PowerPC
IH_ARCH_S390           = 8       # IBM S390
IH_ARCH_SH             = 9       # SuperH
IH_ARCH_SPARC          = 10      # Sparc
IH_ARCH_SPARC64        = 11      # Sparc 64 Bit
IH_ARCH_M68K           = 12      # M68K
IH_ARCH_MICROBLAZE     = 14      # MicroBlaze
IH_ARCH_NIOS2          = 15      # Nios-II
IH_ARCH_BLACKFIN       = 16      # Blackfin
IH_ARCH_AVR32          = 17      # AVR32
IH_ARCH_ST200          = 18      # STMicroelectronics ST200
IH_ARCH_SANDBOX        = 19      # Sandbox architecture (test only)
IH_ARCH_NDS32          = 20      # ANDES Technology - NDS32
IH_ARCH_OPENRISC       = 21      # OpenRISC 1000
IH_ARCH_ARM64          = 22      # ARM64
IH_ARCH_ARC            = 23      # Synopsys DesignWare ARC
IH_ARCH_X86_64         = 24      # AMD x86_64, Intel and Via


#
# Image Types
#
# "Standalone Programs" are directly runnable in the environment
#      provided by U-Boot; it is expected that (if they behave
#      well) you can continue to work in U-Boot after return from
#      the Standalone Program.
# "OS Kernel Images" are usually images of some Embedded OS which
#      will take over control completely. Usually these programs
#      will install their own set of exception handlers, device
#      drivers, set up the MMU, etc. - this means, that you cannot
#      expect to re-enter U-Boot except by resetting the CPU.
# "RAMDisk Images" are more or less just data blocks, and their
#      parameters (address, size) are passed to an OS kernel that is
#      being started.
# "Multi-File Images" contain several images, typically an OS
#      (Linux) kernel image and one or more data images like
#      RAMDisks. This construct is useful for instance when you want
#      to boot over the network using BOOTP etc., where the boot
#      server provides just a single image file, but you want to get
#      for instance an OS kernel and a RAMDisk image.
#
#      "Multi-File Images" start with a list of image sizes, each
#      image size (in bytes) specified by an "uint32_t" in network
#      byte order. This list is terminated by an "(uint32_t)0".
#      Immediately after the terminating 0 follow the images, one by
#      one, all aligned on "uint32_t" boundaries (size rounded up to
#      a multiple of 4 bytes - except for the last file).
#
# "Firmware Images" are binary images containing firmware (like
#      U-Boot or FPGA images) which usually will be programmed to
#      flash memory.
#
# "Script files" are command sequences that will be executed by
#      U-Boot's command interpreter; this feature is especially
#      useful when you configure U-Boot to use a real shell (hush)
#      as command interpreter (=> Shell Scripts).
#
IH_TYPE_INVALID        = 0       # Invalid Image
IH_TYPE_STANDALONE     = 1       # Standalone Program
IH_TYPE_KERNEL         = 2       # OS Kernel Image
IH_TYPE_RAMDISK        = 3       # RAMDisk Image
IH_TYPE_MULTI          = 4       # Multi-File Image
IH_TYPE_FIRMWARE       = 5       # Firmware Image
IH_TYPE_SCRIPT         = 6       # Script file
IH_TYPE_FILESYSTEM     = 7       # Filesystem Image (any type)
IH_TYPE_FLATDT         = 8       # Binary Flat Device Tree Blob
IH_TYPE_KWBIMAGE       = 9       # Kirkwood Boot Image
IH_TYPE_IMXIMAGE       = 10      # Freescale IMXBoot Image
IH_TYPE_UBLIMAGE       = 11      # Davinci UBL Image
IH_TYPE_OMAPIMAGE      = 12      # TI OMAP Config Header Image
IH_TYPE_AISIMAGE       = 13      # TI Davinci AIS Image
IH_TYPE_KERNEL_NOLOAD  = 14      # OS Kernel Image, can run from any load address
IH_TYPE_PBLIMAGE       = 15      # Freescale PBL Boot Image
IH_TYPE_MXSIMAGE       = 16      # Freescale MXSBoot Image
IH_TYPE_GPIMAGE        = 17      # TI Keystone GPHeader Image
IH_TYPE_ATMELIMAGE     = 18      # ATMEL ROM bootable Image
IH_TYPE_SOCFPGAIMAGE   = 19      # Altera SOCFPGA Preloader
IH_TYPE_X86_SETUP      = 20      # x86 setup.bin Image
IH_TYPE_LPC32XXIMAGE   = 21      # x86 setup.bin Image
IH_TYPE_LOADABLE       = 22      # A list of typeless images
IH_TYPE_RKIMAGE        = 23      # Rockchip Boot Image
IH_TYPE_RKSD           = 24      # Rockchip SD card
IH_TYPE_RKSPI          = 25      # Rockchip SPI image

IH_TYPE_COUNT          = 26      # Number of image types


#
# Compression Types
#
IH_COMP_NONE           = 0       #  No   Compression Used
IH_COMP_GZIP           = 1       # gzip  Compression Used
IH_COMP_BZIP2          = 2       # bzip2 Compression Used
IH_COMP_LZMA           = 3       # lzma  Compression Used
IH_COMP_LZO            = 4       # lzo   Compression Used

IH_MAGIC      = 0x27051956      # Image Magic Number
IH_NMLEN              = 32      # Image Name Length

KEY_CHSETTINGS = 0xC0C0C0C1

# Header size is CH header rounded up to 512 bytes plus GP header */
#define GPIMAGE_HDR_SIZE (sizeof(struct gp_header))
#define OMAP_CH_HDR_SIZE 512
#define OMAP_FILE_HDR_SIZE (OMAP_CH_HDR_SIZE + GPIMAGE_HDR_SIZE)


class CHTOC(ctypes.LittleEndianStructure, AccessBytesMixin):
    """
    From: tools/omapimage.h
    struct ch_toc {
            uint32_t section_offset;
            uint32_t section_size;
            uint8_t unused[12];
            uint8_t section_name[12];
    };
    """
    _fields_ = [
        ("section_offset", ctypes.c_uint32),
        ("section_size", ctypes.c_uint32),
        ("unused", ctypes.c_uint8 * 12),
        ("section_name", ctypes.c_uint8 * 12),
        ("closing_item", ctypes.c_uint8 * 32),
    ]

    def verify(self):
        """
        Verify CHTOC by checking the section_name field for a well-known value
        """
        section_name_str = ''.join([chr(c) for c in self.section_name])
        if section_name_str != "CHSETTINGS\x00\x00":
            return False
        else:
            return True


class CHSettings(ctypes.LittleEndianStructure, AccessBytesMixin):
    """
    From: tools/omapimage.h
    struct ch_settings {
            uint32_t section_key;
            uint8_t valid;
            uint8_t version;
            uint16_t reserved;
            uint32_t flags;
    };
    """
    _fields_ = [
        ("section_key", ctypes.c_uint32),
        ("valid", ctypes.c_uint8),
        ("version", ctypes.c_uint8),
        ("reserved", ctypes.c_uint16),
        ("flags", ctypes.c_uint32),
    ]

    def verify(self):
        """
        Verify CHSettings by checking the section_key field for a well-known
        value
        """
        if self.section_key != KEY_CHSETTINGS:
            return False
        else:
            return True


class GPHeader(ctypes.LittleEndianStructure, AccessBytesMixin):
    """
    From: tools/gpheader.h
    struct gp_header {
            uint32_t size;
            uint32_t load_addr;
    };
    """
    _fields_ = [
        ("size", ctypes.c_uint32),
        ("load_addr", ctypes.c_uint32),
    ]

    def verify(self):
        """
        Verify GPHeader by ensuring size and load_addr fields are non-zero
        """
        if self.size == 0 or self.load_addr == 0:
            return False
        else:
            return True


class SPLImageHeader(object):
    """
    Helpful files:
    tools/gpheader.h
    tools/gpimage-common.c
    tools/omapimage.c
    tools/gpimage.c
    """
    def __init__(self, data):
        self.data = data
        self.offset = data.tell()
        # Populate Configuration Header TOC
        self.ch_toc = CHTOC()
        self.ch_toc.set_bytes(self.data.read(ctypes.sizeof(CHTOC)))

        # Populate Configuration Header Settings
        self.ch_settings = CHSettings()
        self.ch_settings.set_bytes(self.data.read(ctypes.sizeof(CHSettings)))

        # Configuration Header is always 512 bytes long.
        # Seek ahead and populate the GP header
        self.data.seek(self.offset + 512)
        self.gp_header = GPHeader()
        self.gp_header.set_bytes(self.data.read(ctypes.sizeof(GPHeader)))

    def verify(self):
        """
        Verify all three header fields.
        """
        return all([self.ch_toc.verify(),
                    self.ch_settings.verify(),
                    self.gp_header.verify()])

    def __len__(self):
        """
        Total length in bytes for a SPLImageHeader
        """
        size = (512 - ctypes.sizeof(CHTOC) - ctypes.sizeof(CHSettings)) # See __init__
        size += ctypes.sizeof(self.ch_toc)
        size += ctypes.sizeof(self.ch_settings)
        size += ctypes.sizeof(self.gp_header)
        return size


class UBootImageHeader(ctypes.BigEndianStructure, AccessBytesMixin):
    """
    /*
     * Legacy format image header,
     * all data in network byte order (aka natural aka bigendian).
     */
    typedef struct image_header {
        __be32          ih_magic;       /* Image Header Magic Number    */
        __be32          ih_hcrc;        /* Image Header CRC Checksum    */
        __be32          ih_time;        /* Image Creation Timestamp     */
        __be32          ih_size;        /* Image Data Size              */
        __be32          ih_load;        /* Data  Load  Address          */
        __be32          ih_ep;          /* Entry Point Address          */
        __be32          ih_dcrc;        /* Image Data CRC Checksum      */
        uint8_t         ih_os;          /* Operating System             */
        uint8_t         ih_arch;        /* CPU architecture             */
        uint8_t         ih_type;        /* Image Type                   */
        uint8_t         ih_comp;        /* Compression Type             */
        uint8_t         ih_name[IH_NMLEN];      /* Image Name           */
    } image_header_t;
    """
    _fields_ = [
        ("ih_magic", ctypes.c_uint32),
        ("ih_hcrc", ctypes.c_uint32),
        ("ih_time", ctypes.c_uint32),
        ("ih_size", ctypes.c_uint32),
        ("ih_load", ctypes.c_uint32),
        ("ih_ep", ctypes.c_uint32),
        ("ih_dcrc", ctypes.c_uint32),
        ("ih_os", ctypes.c_uint8),
        ("ih_arch", ctypes.c_uint8),
        ("ih_type", ctypes.c_uint8),
        ("ih_comp", ctypes.c_uint8),
        ("ih_name", ctypes.c_uint8 * IH_NMLEN),
    ]

    def verify(self):
        if ( self.ih_magic == IH_MAGIC and
             self.ih_hcrc == self.calculate_hcrc()):
            return True
        else:
            return False

    def calculate_hcrc(self):
        """
        Calculates the header CRC using CRC32. Operates on a copy.
        """
        copy = UBootImageHeader()
        copy.set_bytes(self.get_bytes())
        copy.ih_hcrc = 0x00000000

        header_crc = binascii.crc32(copy.get_bytes(), 0)

        return header_crc & 0xffffffff

    def __len__(self):
        return ctypes.sizeof(self)


class SPLImage(object):
    """

    """
    def __init__(self, data):
        self.data = data
        self.start_offset = data.tell()
        self.image_data = StringIO.StringIO()

        self.header = SPLImageHeader(data)

    def verify_header(self):
        return self.header.verify()

    def verify_image(self):
        return True

    def get_image(self):
        self.data.seek(self.start_offset + len(self.header))

        for chunk in chunk_file_reader(self.data, self.header.gp_header.size, 1024):
            self.image_data.write(chunk)

    def get_data(self):
        yield self.header.get_bytes()
        for chunk in chunk_file_reader(self.image_data,
                                       self.header.gp_header.size,
                                       1024):
            yield chunk

    def print_image(self):
        print "OMAP SPL Image"
        print "Location: %s" % self.start_offset
        print "Size: %s" % self.header.gp_header.size
        print "Load Address: %08x" % self.header.gp_header.load_addr

    def __len__(self):
        """
        Returns the total length of the SPL image in bytes
        """
        return (self.header.gp_header.size + len(self.header))


class UBootImage(object):
    def __init__(self, data):
        """

        """
        self.data = data
        self.start_offset = data.tell()
        self.image_data = StringIO.StringIO()
        self.image_crc = 0

        # Read in header and advance file pointer to start of image data
        self.header = UBootImageHeader()
        self.header.set_bytes(self.data.read(ctypes.sizeof(UBootImageHeader)))

    def verify_header(self):
        return self.header.verify()

    def verify_image(self):
        return self.header.ih_dcrc == self.image_crc

    def get_image(self):
        self.data.seek(self.start_offset + ctypes.sizeof(UBootImageHeader))

        for chunk in chunk_file_reader(self.data, self.header.ih_size, 1024):
            self.image_data.write(chunk)
            self.image_crc = binascii.crc32(chunk, self.image_crc)

        self.image_crc &= 0xffffffff

    def get_data(self):
        yield self.header.get_bytes()
        for chunk in chunk_file_reader(self.image_data,
                                       self.header.ih_size,
                                       1024):
            yield chunk

    def print_image(self):
        print "U-Boot Legacy Image"
        print "Location: %s" % self.start_offset
        print "Size: %s" % self.header.ih_size
        print "Load Address: %08x" % self.header.ih_load
        print "Name: %s"  % ''.join([chr(c) for c in self.header.ih_name])

    def __len__(self):
        """
        Returns the total length of the U-Boot image in bytes
        """
        return (self.header.ih_size + len(self.header))


class OMAPDiskInspector(object):
    """
    26.1.7.5.5 MMC/SD Read Sector Procedure in Raw Mode:
    In raw mode the booting image can be located at one of the four consecutive
    locations in the main area:
      offset 0x0 / 0x20000 (128 KB) / 0x40000 (256 KB) / 0x60000 (384 KB)
    """
    offsets = [0x00000, 0x20000, 0x40000, 0x60000]

    def __init__(self, disk_path, image_types):
        self.disk_path = disk_path
        self.image_types = image_types

    def inspect(self):
        images = []

        for image_cls, offset in itertools.product(self.image_types,
                                                   self.offsets):
            with open(self.disk_path) as disk_handle:
                disk_handle.seek(offset)
                image = image_cls(disk_handle)

                if image.verify_header():
                    image.get_image()
                    if image.verify_image():
                        images.append(image)

        return images
