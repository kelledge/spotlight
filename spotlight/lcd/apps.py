import threading
import time

from spotlight.lib.mm.manager import get_modem_manager_object, get_system_bus, Manager

att = ''.join([
	chr(0b11111),
	chr(0b01001),
	chr(0b00101),
	chr(0b00011),
	chr(0b01001),
	chr(0b10101),
	chr(0b11101),
	chr(0b10101),
])

vzw = ''.join([
	chr(0b11111),
	chr(0b01001),
	chr(0b00101),
	chr(0b00011),
	chr(0b10101),
	chr(0b10101),
	chr(0b10101),
	chr(0b01001),
])

four_bars = ''.join([
    chr(0b11111),
	chr(0b11111),
	chr(0b11110),
	chr(0b11110),
	chr(0b11100),
	chr(0b11100),
	chr(0b11000),
	chr(0b11000),
])

three_bars = ''.join([
    chr(0b00000),
	chr(0b00000),
	chr(0b11110),
	chr(0b11110),
	chr(0b11100),
	chr(0b11100),
	chr(0b11000),
	chr(0b11000),
])

two_bars = ''.join([
    chr(0b00000),
	chr(0b00000),
	chr(0b00000),
	chr(0b00000),
	chr(0b11100),
	chr(0b11100),
	chr(0b11000),
	chr(0b11000),
])

one_bars = ''.join([
    chr(0b00000),
	chr(0b00000),
	chr(0b00000),
	chr(0b00000),
	chr(0b00000),
	chr(0b00000),
	chr(0b11000),
	chr(0b11000),
])

no_bars = ''.join([
    chr(0b00000),
	chr(0b00000),
	chr(0b00000),
	chr(0b00000),
	chr(0b00000),
	chr(0b00000),
	chr(0b00000),
	chr(0b00000),
])


class CellularState(threading.Thread):
    def __init__(self, lcd, modem):
        super(CellularState, self).__init__()

        self.lcd = lcd
        self.modem = modem

        self.state = lcd.request_display_segment(0, 1)
        self.state_char = lcd.request_character(0)

        self.state.seek(0)
        self.state.write('\x00')
        self.state_char.write(att)

        self.signal = lcd.request_display_segment(1, 1)
        self.signal_char = lcd.request_character(1)

        self.signal.seek(0)
        self.signal.write('\x01')

    def run(self):
        while True:
            signal_quality = self.modem.SignalQuality
            print signal_quality
            self.signal_char.seek(0)
            if signal_quality <= 100 and signal_quality > 80:
                self.signal_char.write(four_bars)

            elif signal_quality <= 80 and signal_quality > 60:
                self.signal_char.write(three_bars)

            elif signal_quality <= 60 and signal_quality > 40:
                self.signal_char.write(two_bars)

            elif signal_quality <= 40 and signal_quality > 20:
                self.signal_char.write(one_bars)

            elif signal_quality <= 20 and signal_quality > 0:
                self.signal_char.write(no_bars)

            else:
                self.signal_char.write(no_bars)

            time.sleep(1)


class Uptime(threading.Thread):
    def __init__(self, lcd):
        super(Uptime, self).__init__()

        self.lcd = lcd
        self.uptime = lcd.request_display_segment(8, 8)

    def run(self):
        while True:
            with open('/proc/uptime') as handle:
                up = float(handle.read().split(' ')[0])

            print up
            self.uptime.seek(0)
            self.uptime.write("%8s" % int(up))
            time.sleep(1)
