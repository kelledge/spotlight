"""
A very basic virtual memory implementation.

This module's primary purpose is to handle fragmented memory
"""
import itertools


class VirtualFile(object):
    """
    Currently has tons of issues:
     1) Does not cleanly handle EOF behavior
     2) Does not keep file position state independent of target. Is issue when
        you have multiple virtual files mapped to a single target. Writing to
        one virtual file update the file position on all other virtual files.
    """
    def __init__(self, mapper, target):
        self.mapper = mapper
        self.target = target

    def seek(self, virtual_address):
        target_address = self.mapper.to_target_address(virtual_address)
        if target_address is not None:
            self.target.seek(target_address)


    def tell(self):
        target_address = self.target.tell()
        virtual_address = self.mapper.to_virtual_address(target_address)
        return virtual_address

    def read(self, length=1):
        bytes = []
        for _ in range(length):
            position = self.tell()
            if position is None:
                break

            bytes.append(self.target.read(1))
            position += 1
            self.seek(position)

        return ''.join(bytes)

    def write(self, buf):
        for b in buf:
            position = self.tell()
            if position is None:
                break

            self.target.write(b)
            position += 1
            self.seek(position)


class VirtualMapper(object):
    def __init__(self):
        self.segment_table = []
        self.length = 0

    def add_segment(self, target_segment):
        """
        Adds an address translation rule.
        """
        mapper_end_address = self.length
        segment_length = len(target_segment)
        self.length += segment_length

        virtual_segment = MemorySegment(mapper_end_address, segment_length)
        self.segment_table.append((virtual_segment, target_segment))

    def to_virtual_address(self, target_address):
        for virtual, target in self.segment_table:
            if target_address in target:
                offset = target.offset_of(target_address)
                virtual_address = virtual.address_of(offset)
                return virtual_address

        return None

    def to_target_address(self, virtual_address):
        for virtual, target in self.segment_table:
            if virtual_address in virtual:
                offset = virtual.offset_of(virtual_address)
                target_address = target.address_of(offset)
                return target_address

        return None


class AddressTranslator(object):
    """
    A translator defining a map between target and virtual memory regions.
    """
    def __init__(self, virtual_region, target_region):
        if len(virtual_region) != len(target_region):
            raise ValueError("%r and %r are not the same size" % (virtual_region, target_region))

        self.virtual_region = virtual_region
        self.target_region = target_region

    def __iter__(self):
        return itertools.izip(self.virtual_region, self.target_region)

    def to_virtual_address(self, target_address):
        offset = self.target_region.offset_of(target_address)
        virtual_address = self.virtual_region.address_of(offset)

        return virtual_address

    def to_target_address(self, virtual_address):
        offset = self.virtual_region.offset_of(virtual_address)
        target_address = self.target_region.address_of(offset)

        return target_address


class MemorySegment(object):
    """
    A contiguous region of addressable memory. This is the atomic unit for
    defining mappings between memory segments.
    """
    def __init__(self, address, length):
        if length <= 0:
            raise ValueError("Length must be >= 1")

        if address < 0:
            raise ValueError("Address can not be negative")

        self.length = length
        self.address_start = address
        self.address_end = address + length - 1

    def __iter__(self):
        return iter(xrange(self.address_start, self.address_end + 1))

    def __contains__(self, address):
        if (address >= self.address_start and
            address <= self.address_end):
            return True
        else:
            return False

    def __len__(self):
        return self.length

    def __repr__(self):
        return "MemoryRegion(address=0x%08x, length=%d)" % (self.address_start, self.length)

    def offset_of(self, address):
        """
        Given an address, attempt to return the offset within the segment.
        """
        if address not in self:
            raise ValueError("Address %d is not in %r" % (address, self))

        offset = address - self.address_start
        return offset

    def address_of(self, offset):
        """
        Given an offset, attempt to return the address of this offset.
        """
        if offset < 0 or offset >= self.length:
            raise ValueError("Offset %d is not in %r" % (offset, self))

        address = self.address_start + offset
        return address
