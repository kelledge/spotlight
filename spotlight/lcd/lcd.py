import os
from spotlight.lcd import vm

class LCDSysfs(object):
    """
    Wrapper for the LCD sysfs attributes.
    """
    def __init__(self, device):
        self.device = device

        self._ddram =  open(os.path.join(self.device.sys_path, 'ddram'), 'r+b')
        self._cgram =  open(os.path.join(self.device.sys_path, 'cgram'), 'r+b')

    @property
    def dimension(self):
        with open(os.path.join(self.device.sys_path, 'rows')) as handle:
            rows = int(handle.read())

        with open(os.path.join(self.device.sys_path, 'columns')) as handle:
            columns = int(handle.read())

        return (rows, columns)

    @property
    def cursor(self):
        cursor_path = os.path.join(self.device.sys_path, 'cursor')

        with open(cursor_path) as handle:
            state = handle.read()

        return state

    @cursor.setter
    def cursor(self, new_state):
        cursor_path = os.path.join(self.device.sys_path, 'cursor')

        if new_state == True: # I don't care if this is not pythonic.
            state = 'ON'
        elif new_state == False:
            state = 'OFF'
        else:
            raise ValueError('Status must be True or False')

        with open(cursor_path, 'r+b') as handle:
            handle.write(state)

    @property
    def ddram(self):
        return self._ddram

    @property
    def cgram(self):
        return self._cgram


class Lcd(object):
    def __init__(self, lcd_sysfs):
        self.lcd_sysfs = lcd_sysfs

        mapper = vm.VirtualMapper()

        top_row = vm.MemorySegment(0x00, 8)
        mapper.add_segment(top_row)

        bottom_row = vm.MemorySegment(0x28, 8)
        mapper.add_segment(bottom_row)

        self.ddram = vm.VirtualFile(mapper, self.lcd_sysfs.ddram)
        self.cgram = self.lcd_sysfs.cgram

    def request_display_segment(self, address, length):
        segment = vm.MemorySegment(address, length)
        ddram_mapper = vm.VirtualMapper()
        ddram_mapper.add_segment(segment)

        return vm.VirtualFile(ddram_mapper, self.ddram)

    def request_character(self, index):
        address = index * 8
        length = 8

        cgram_mapper = vm.VirtualMapper()
        segment = vm.MemorySegment(address, length)
        cgram_mapper.add_segment(segment)

        return vm.VirtualFile(cgram_mapper, self.cgram)
