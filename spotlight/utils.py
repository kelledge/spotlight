"""
Helper utilites
"""
import ctypes


class AccessBytesMixin(object):
    """
    A mixin for use with ctype structures that allows for getting and
    setting the underlaying data.
    """
    def get_bytes(self):
        """
        Gets the structure "value" as a raw byte string
        """
        return buffer(self)[:]

    def set_bytes(self, bytes):
        """
        Set the raw data for this structure.
        """
        fit = min(len(bytes), ctypes.sizeof(self))
        ctypes.memmove(ctypes.addressof(self), bytes, fit)

    @classmethod
    def from_bytes(cls, bytes):
        structure = cls()
        structure.set_bytes(bytes)
        return structure


def chunk_size_generator(total_length, chunk_size):
    """
    Breaks a length into corrisponding chunk sizes.
    """
    if chunk_size == 0:
        raise ValueError("chunk_size must not be 0")

    quotient, remainder = divmod(total_length, chunk_size)
    for _ in xrange(quotient):
        yield chunk_size
    if remainder:
        yield remainder


def chunk_file_reader(file_handle, total_length, chunk_size=1024):
    """
    Reads a given length of data in chunked sizes from a file
    """
    for chunk_size in chunk_size_generator(total_length, chunk_size):
        chunk = file_handle.read(chunk_size)
        if not chunk:
            break
        yield chunk


def field_as_string(c_array, length=0, null_terminated=False):
    """
    Simple helper function for interpreting C style strings from ctype arrays
    """
    length = length or ctypes.sizeof(c_array)
    string = ''.join([chr(c) for c in c_array])

    if null_terminated == True:
        string = string.partition('\x00')[0]

    return string

# http://stackoverflow.com/a/1695250
def enum(*sequential, **named):
    enums = dict(zip(sequential, range(len(sequential))), **named)
    reverse = dict((value, key) for key, value in enums.iteritems())
    enums['reverse_mapping'] = reverse
    return type('Enum', (), enums)
