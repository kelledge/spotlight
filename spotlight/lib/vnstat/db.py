"""
vnStat Database interface
"""
import datetime
import ctypes
import os
import glob

from spotlight.utils import AccessBytesMixin


DEFAULT_DATABASE_DIRECTORY = '/var/lib/vnstat/'


# I *think* c_time is usually uint64, but I'm not sure
c_time = ctypes.c_uint32


def list_files(glob_pattern):
    rel_file_paths  = glob.glob(glob_pattern)
    abs_file_paths = [os.path.abspath(f) for f in files]

    return abs_file_paths


class SystemDatabase(object):
    """

    """
    def __init__(self, interfaces):
        self.db_directory = db_directory

    def reload(self):
        pass


class InterfaceDatabase(object):
    """

    """
    def __init__(self, interface_data):
        self.interface_data = interface_data


class Hour(ctypes.Structure, AccessBytesMixin):
    """
    typedef struct {
        time_t date;
        uint64_t rx, tx;
    } HOUR;

    "rx" and "tx" fields are stored in kilobytes.
    """
    _fields_ = [
        ("_date", c_time),
        ("_rx", ctypes.c_uint64), # Unit: KB
        ("_tx", ctypes.c_uint64), # Unit: KB
    ]

    @property
    def rx(self):
        return self._rx * 1024

    @property
    def tx(self):
        return self._tx * 1024

    @property
    def total(self):
        return self.tx + self.rx

    @property
    def date(self):
        return datetime.datetime.utcfromtimestamp(self._date)

    def __repr__(self):
        return "Hour(date='{}', rx={}, tx={}, total={})".format(
            self.date,
            self.rx,
            self.tx,
            self.total
        )

class Day(ctypes.Structure, AccessBytesMixin):
    """
    typedef struct {
        time_t date;
        uint64_t rx, tx;
        int rxk, txk;
        int used;
    } DAY;

    "rx" and "tx" fields are in bytes.
    "rxk", and "txk" are in kilobytes.

    When "rx" or "tx" overflow 1024 (1KB), the corresponding kilobyte counter is
    incremented by one and the remainder is left in the byte field.

    To interpret these fields as bytes, the following equation is applied:
      RX: rx + rxk * 1024
      TX: tx + txk * 1024
    """
    _fields_ = [
        ("_date", c_time),
        ("_rx", ctypes.c_uint64),
        ("_tx", ctypes.c_uint64),
        ("rxk", ctypes.c_int),
        ("txk", ctypes.c_int),
        ("_used", ctypes.c_int),
    ]

    @property
    def rx(self):
        return self._rx + self.rxk * 1024

    @property
    def tx(self):
        return self._tx + self.txk * 1024

    @property
    def date(self):
        return datetime.datetime.utcfromtimestamp(self._date)

    @property
    def used(self):
        if self._used == 1:
            return True
        elif self._used == 0:
            return False
        else:
            raise ValueError('"used" field must be 0 or 1')

    @property
    def total(self):
        return self.tx + self.rx

    def __repr__(self):
        return "Day(date='{}', rx={}, tx={}, total={}, used={})".format(
            self.date,
            self.rx,
            self.tx,
            self.total,
            self.used
        )

class Month(ctypes.Structure, AccessBytesMixin):
    """
    typedef struct {
        time_t month;
        uint64_t rx, tx;
        int rxk, txk;
        int used;
    } MONTH;

    "rx" and "tx" fields are in bytes.
    "rxk", and "txk" are in kilobytes.

    When "rx" or "tx" overflow 1024 (1KB), the corresponding kilobyte counter is
    incremented by one and the remainder is left in the byte field.

    To interpret these fields as bytes, the following equation is applied:
      RX: rx + rxk * 1024
      TX: tx + txk * 1024
    """
    _fields_ = [
        ("month", c_time),
        ("rx", ctypes.c_uint64),
        ("tx", ctypes.c_uint64),
        ("rxk", ctypes.c_int),
        ("txk", ctypes.c_int),
        ("used", ctypes.c_int),
    ]


class Database(ctypes.Structure, AccessBytesMixin):
    """
    typedef struct {
        int version;
        char interface[32];
        char nick[32];
        int active;
        uint64_t totalrx, totaltx, currx, curtx;
        int totalrxk, totaltxk;
        time_t lastupdated, created;
        DAY day[30];
        MONTH month[12];
        DAY top10[10];
        HOUR hour[24];
        uint64_t btime;
    } DATA;
    """
    _fields_ = [
        ("version", ctypes.c_int),
        ("interface", ctypes.c_char * 32),
        ("nick", ctypes.c_char * 32),
        ("active", ctypes.c_int),
        ("totalrx", ctypes.c_uint64),
        ("totaltx", ctypes.c_uint64),
        ("currrx", ctypes.c_uint64),
        ("currtx", ctypes.c_uint64),
        ("totalrxk", ctypes.c_int),
        ("totaltxk", ctypes.c_int),
        ("_lastupdated", c_time),
        ("_created", c_time),
        ("day", Day * 30),
        ("month", Month * 12),
        ("top10", Day * 10),
        ("hour", Hour * 24),
        ("btime", ctypes.c_uint64),
    ]

    @property
    def lastupdated(self):
        return datetime.datetime.utcfromtimestamp(self._lastupdated)

    @property
    def created(self):
        return datetime.datetime.utcfromtimestamp(self._created)
