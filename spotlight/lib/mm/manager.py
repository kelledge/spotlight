import dispatch

from twisted.internet import defer

from spotlight.lib.mm.modem import Modem
from spotlight.dbus.bus import getSystemBus

from spotlight.dbus.object_manager import RemoteObjectManager

MM_BUS_NAME = 'org.freedesktop.ModemManager1'
MM_OBJ_NAME = '/org/freedesktop/ModemManager1'
MM_IFACE_NAME = 'org.freedesktop.ModemManager1'


def getModemManagerObject(systemBus):
    return systemBus.getRemoteObject(
        MM_BUS_NAME,
        MM_OBJ_NAME
    )


class ModemManager(object):
    """

    """
    def __init__(self, objectManager):
        self.objectManager = objectManager

        self.managedModems = {}
        self.cacheLock = defer.DeferredLock()

        self.objectManager.objectAdded.connect(self.onModemAdded)
        self.objectManager.objectRemoved.connect(self.onModemRemoved)

        self.modemAdded = dispatch.Signal(providing_args=["modem"])
        self.modemRemoved = dispatch.Signal(providing_args=["modem"])

    #@defer.inlineCallbacks
    def connect(self):
        #yield self.objectManager.connect() # ERROR: This is called here and in `fromConnection`
        modems = self.objectManager.getManagedObjects()
        for modemPath, modemProxy in modems.iteritems():
            self.managedModems[modemPath] = Modem(modemProxy)

    def getManagedModems(self):
        return self.managedModems

    def onModemAdded(self, sender, **kwargs):
        print 'onModemAdded'
        try:
            self.cacheLock.acquire()

            objectPath = kwargs['objectPath']
            objectProxy = kwargs['objectProxy']
            print objectPath, objectProxy
            modem = Modem(objectPath)

            self.managedModems[objectProxy] = modem
            self.modemAdded.send(
                sender=self,
                modem=modem
            )
        except KeyError as e:
            print 'WARN:', e.message
        finally:
            self.cacheLock.release()

    def onModemRemoved(self, sender, **kwargs):
        print 'onModemRemoved'
        try:
            self.cacheLock.acquire()
            objectPath = kwargs['objectPath']
            objectProxy = kwargs['objectProxy']
            print objectPath, objectProxy
            modem = self.managedModems.pop(objectPath)

            self.modemRemoved.send(
                sender=self,
                modem=modem
            )
        except KeyError as e:
            print 'WARN:', e.message
        finally:
            self.cacheLock.release()

    @classmethod
    @defer.inlineCallbacks
    def fromConnection(cls, connection):
        objectManager = yield RemoteObjectManager.fromObjectPath(
            connection,
            MM_BUS_NAME,
            MM_OBJ_NAME
        )
        obj = cls(objectManager)
        defer.returnValue(obj)
