"""
org.freedesktop.ModemManager1.Sim
"""

class Sim(object):
    """
    SimIdentifier       readable   s
    Imsi                readable   s
    OperatorIdentifier  readable   s
    OperatorName        readable   s
    """
    def __init__(self, dbus_object):
        pass

    @property
    def SimIdentifier(self):
        pass

    @property
    def Imsi(self):
        pass

    @property
    def OperatorIdentifier(self):
        pass

    @property
    def OperatorName(self):
        pass
