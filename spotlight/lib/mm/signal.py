"""
org.freedesktop.ModemManager1.Modem.Signal
"""

class Signal(object):
    """
    Methods:
    Setup (IN  u rate);

    Properties:
    Rate  readable   u
    Cdma  readable   a{sv}
    Evdo  readable   a{sv}
    Gsm   readable   a{sv}
    Umts  readable   a{sv}
    Lte   readable   a{sv}
    """
    def __init__(self, dbusObject):
        self.dbusObject = dbusObject

    def Setup(self, rate):
        pass

    @property
    def Rate(self):
        pass

    @property
    def Cdma(self):
        pass

    @property
    def Evdo(self):
        pass

    @property
    def Gsm(self):
        pass

    @property
    def Umts(self):
        pass

    @property
    def Lte(self):
        pass
