"""
Simple wrappers for the 'org.freedesktop.ModemManager1.Modem*' DBus interfaces.

This is a pretty brute-force implementation, but this is very simple and easy to
understand. The ModemManager1 DBus interface is pretty stable, and these
bindings should only rarely need updating.
"""

MODEM_INTERFACE = 'org.freedesktop.ModemManager1.Modem'
SIM_INTERFACE = 'org.freedesktop.ModemManager1.Sim'
SIGNAL_INTERFACE = 'org.freedesktop.ModemManager1.Modem.Signal'
PROPERTIES_INTERFACE = 'org.freedesktop.DBus.Properties'

class Modem(object):
    """
    Signals:
    StateChanged (i old, i new, u reason);

    Properties:
    Sim                    readable   o
    Bearers                readable   ao
    SupportedCapabilities  readable   au
    CurrentCapabilities    readable   u
    MaxBearers             readable   u
    MaxActiveBearers       readable   u
    Manufacturer           readable   s
    Model                  readable   s
    Revision               readable   s
    DeviceIdentifier       readable   s
    Device                 readable   s
    Drivers                readable   as
    Plugin                 readable   s
    PrimaryPort            readable   s
    Ports                  readable   a(su)
    EquipmentIdentifier    readable   s
    UnlockRequired         readable   u
    UnlockRetries          readable   a{uu}
    State                  readable   i
    StateFailedReason      readable   u
    AccessTechnologies     readable   u
    SignalQuality          readable   (ub)
    OwnNumbers             readable   as
    PowerState             readable   u
    SupportedModes         readable   a(uu)
    CurrentModes           readable   (uu)
    SupportedBands         readable   au
    CurrentBands           readable   au
    SupportedIpFamilies    readable   u
    """
    def __init__(self, dbusProxy):
        self.dbusProxy = dbusProxy

    def getProperty(self, propertyName):
        return self.dbusProxy.callRemote(
            'Get',
            'org.freedesktop.ModemManager1.Modem',
            propertyName,
            interface='org.freedesktop.DBus.Properties'
        )

    def onStateChange(self, old, new, reason):
        pass

    @property
    def Sim(self):
        pass

    @property
    def SignalQuality(self):
        return self.getProperty('SignalQuality')

    @property
    def Manufacturer(self):
        return self.getProperty('Manufacturer')

    @property
    def Model(self):
        return self.getProperty('Model')


class Modem3gpp(object):
    """
    Properties:
    Imei                  readable   s
    RegistrationState     readable   u
    OperatorCode          readable   s
    OperatorName          readable   s
    EnabledFacilityLocks  readable   u
    SubscriptionState     readable   u
    """

class ModemCdma(object):
    """
    Properties:
    ActivationState          readable   u
    Meid                     readable   s
    Esn                      readable   s
    Sid                      readable   u
    Nid                      readable   u
    Cdma1xRegistrationState  readable   u
    EvdoRegistrationState    readable   u
    """
