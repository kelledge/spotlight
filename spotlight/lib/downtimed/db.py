import ctypes
import datetime
import itertools

from spotlight.utils import AccessBytesMixin, enum


DEFAULT_DATABASE_DIRECTORY = "/var/lib/downtimed/"


What = enum(
    NONE     = 0,
    UP       = 1,
    SHUTDOWN = 2,
    CRASH    = 3,
)


def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = itertools.tee(iterable)
    next(b, None)
    return itertools.izip(a, b)


class DatabaseEvent(ctypes.BigEndianStructure, AccessBytesMixin):
    """
    The structure of the downtime database file.

    The format is Y2K38 bug, 64 bit and endianness safe.
    We need a portable format, because everything in the system could
    change during the downtime. Someone could change the machine from
    big-endian 32 bit architecture to 64 bit little-endian computer.
    Or someone might want to transfer the database from one computer to
    another for further analysis. It would also be possible to transmit
    the data in the same format to a remote collector through a network
    socket.

    At the time when 128 bit computers are introduced, possibly some pack
    #pragmas or similar should be inserted here to retain compatibility. XXX

    struct downtimedb {
    	uint8_t	what;		/* Op code of the recorded event  */
    	uint8_t	_padding[7];	/* Reserved for future extensions */
    	int64_t	when;		/* UNIX time in big-endian format */
    };
    """
    _fields_ = [
        ("what", ctypes.c_uint8),
        ("_padding", ctypes.c_uint8 * 7),
        ("_when", ctypes.c_uint64),
    ]

    @property
    def when(self):
        """
        Convert timestamp to datetime object.
        """
        return datetime.datetime.utcfromtimestamp(self._when)

    def __repr__(self):
        return "DatabaseEvent(what='{}', when='{}')".format(
            What.reverse_mapping[self.what],
            self.when)


class DatabaseEventParser(object):
    def __init__(self):
        self.current_state = None

    def add_event(self, event):
        """
        Event parser state machine.
        """
        if self.current_state is None:
            self.current_state = event.what
        elif self.current_state == What.UP:
            next_state = [What.SHUTDOWN, What.CRASH]
        elif self.current_state == What.SHUTDOWN:
            pass
        elif self.current_state == What.CRASH:
            pass
        elif self.current_state == What.NONE:
            pass
        else:
            pass

class Database(object):
    """
    Downtimed database. This database is a set of {DatabaseEvent} objects.

    Currently assumes that these records are ordered sequencially in the file.
    Does no validation on events.
    """
    def __init__(self, data):
        self.data = data
        self.records = []

        # Get file size
        self.data.seek(0, 2)
        self.size_bytes = self.data.tell()
        self.data.seek(0)

        q, r = divmod(self.size_bytes, ctypes.sizeof(DatabaseEvent))

        # Warn if the database file has a partial records in it
        if (r != 0):
            print 'WARN'

        for _ in range(q):
            record_bytes = self.data.read(ctypes.sizeof(DatabaseEvent))
            record = DatabaseEvent.from_bytes(record_bytes)
            self.records.append(record)

    def select(self, from_time, to_time):
        for record in self:
            if from_time <= record.when <= to_time:
                yield record

    def times(self):
        def down_type(event1, event2):
            if event1.what == What.UP:
                return 'UPTIME', event2.when - event1.when
            elif event2.what == What.UP:
                return 'DOWNTIME', event2.when - event1.when

        uptime = datetime.timedelta(0)
        downtime = datetime.timedelta(0)

        for pair in pairwise(self):
            e_type, length = down_type(pair[0], pair[1])
            if e_type == 'UPTIME':
                uptime += length
            elif e_type == 'DOWNTIME':
                downtime += length

        return uptime, downtime

    def __iter__(self):
        return iter(self.records)

    def __len__(self):
        return len(self.records)
