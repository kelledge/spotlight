"""

"""
from inspect import cleandoc


class NMState(object):
    """
    NMState values indicate the current overall networking state.

    @NM_STATE_UNKNOWN: networking state is unknown
    @NM_STATE_ASLEEP: networking is not enabled
    @NM_STATE_DISCONNECTED: there is no active network connection
    @NM_STATE_DISCONNECTING: network connections are being cleaned up
    @NM_STATE_CONNECTING: a network connection is being started
    @NM_STATE_CONNECTED_LOCAL: there is only local IPv4 and/or IPv6 connectivity
    @NM_STATE_CONNECTED_SITE: there is only site-wide IPv4 and/or IPv6 connectivity
    @NM_STATE_CONNECTED_GLOBAL: there is global IPv4 and/or IPv6 Internet connectivity
    """
    NM_STATE_UNKNOWN          = 0
    NM_STATE_ASLEEP           = 10
    NM_STATE_DISCONNECTED     = 20
    NM_STATE_DISCONNECTING    = 30
    NM_STATE_CONNECTING       = 40
    NM_STATE_CONNECTED_LOCAL  = 50
    NM_STATE_CONNECTED_SITE   = 60
    NM_STATE_CONNECTED_GLOBAL = 70


class NMDeviceType(object):
    """

    """


class NMDeviceCapabilities(object):
    """

    """

class NMDeviceWifiCapabilities(object):
    """

    """


class NM80211ApFlags(object):
    """

    """


class NM80211ApSecurityFlags(object):
    """

    """


class NM80211Mode(object):
    """

    """


class NMBluetoothCapabilities(object):
    """

    """


class NMDeviceModemCapabilities(object):
    """

    """


class NMDeviceState(object):
    """
    @NM_DEVICE_STATE_UNKNOWN: the device's state is unknown
    @NM_DEVICE_STATE_UNMANAGED: the device is recognized, but not managed by NetworkManager
    @NM_DEVICE_STATE_UNAVAILABLE: the device is managed by NetworkManager, but is not available for use.  Reasons may include the wireless switched off, missing firmware, no ethernet carrier, missing supplicant or modem manager, etc.
    @NM_DEVICE_STATE_DISCONNECTED: the device can be activated, but is currently idle and not connected to a network.
    @NM_DEVICE_STATE_PREPARE: the device is preparing the connection to the network.  This may include operations like changing the MAC address, setting physical link properties, and anything else required to connect to the requested network.
    @NM_DEVICE_STATE_CONFIG: the device is connecting to the requested network. This may include operations like associating with the WiFi AP, dialing the modem, connecting to the remote Bluetooth device, etc.
    @NM_DEVICE_STATE_NEED_AUTH: the device requires more information to continue connecting to the requested network.  This includes secrets like WiFi passphrases, login passwords, PIN codes, etc.
    @NM_DEVICE_STATE_IP_CONFIG: the device is requesting IPv4 and/or IPv6 addresses and routing information from the network.
    @NM_DEVICE_STATE_IP_CHECK: the device is checking whether further action is required for the requested network connection.  This may include checking whether only local network access is available, whether a captive portal is blocking access to the Internet, etc.
    @NM_DEVICE_STATE_SECONDARIES: the device is waiting for a secondary connection (like a VPN) which must activated before the device can be activated
    @NM_DEVICE_STATE_ACTIVATED: the device has a network connection, either local or global.
    @NM_DEVICE_STATE_DEACTIVATING: a disconnection from the current network connection was requested, and the device is cleaning up resources used for that connection.  The network connection may still be valid.
    @NM_DEVICE_STATE_FAILED: the device failed to connect to the requested network and is cleaning up the connection request
    """
    NM_DEVICE_STATE_UNKNOWN      = 0
    NM_DEVICE_STATE_UNMANAGED    = 10
    NM_DEVICE_STATE_UNAVAILABLE  = 20
    NM_DEVICE_STATE_DISCONNECTED = 30
    NM_DEVICE_STATE_PREPARE      = 40
    NM_DEVICE_STATE_CONFIG       = 50
    NM_DEVICE_STATE_NEED_AUTH    = 60
    NM_DEVICE_STATE_IP_CONFIG    = 70
    NM_DEVICE_STATE_IP_CHECK     = 80
    NM_DEVICE_STATE_SECONDARIES  = 90
    NM_DEVICE_STATE_ACTIVATED    = 100
    NM_DEVICE_STATE_DEACTIVATING = 110
    NM_DEVICE_STATE_FAILED       = 120



class NMDeviceStateReason(object):
    """
    Device state change reason codes

	@NM_DEVICE_STATE_REASON_NONE: No reason given
	@NM_DEVICE_STATE_REASON_UNKNOWN: Unknown error
	@NM_DEVICE_STATE_REASON_NOW_MANAGED: Device is now managed
	@NM_DEVICE_STATE_REASON_NOW_UNMANAGED: Device is now unmanaged
	@NM_DEVICE_STATE_REASON_CONFIG_FAILED: The device could not be readied for configuration
	@NM_DEVICE_STATE_REASON_IP_CONFIG_UNAVAILABLE: IP configuration could not be reserved (no available address, timeout, etc)
	@NM_DEVICE_STATE_REASON_IP_CONFIG_EXPIRED: The IP config is no longer valid
	@NM_DEVICE_STATE_REASON_NO_SECRETS: Secrets were required, but not provided
	@NM_DEVICE_STATE_REASON_SUPPLICANT_DISCONNECT: 802.1x supplicant disconnected
	@NM_DEVICE_STATE_REASON_SUPPLICANT_CONFIG_FAILED: 802.1x supplicant configuration failed
	@NM_DEVICE_STATE_REASON_SUPPLICANT_FAILED: 802.1x supplicant failed
	@NM_DEVICE_STATE_REASON_SUPPLICANT_TIMEOUT: 802.1x supplicant took too long to authenticate
	@NM_DEVICE_STATE_REASON_PPP_START_FAILED: PPP service failed to start
	@NM_DEVICE_STATE_REASON_PPP_DISCONNECT: PPP service disconnected
	@NM_DEVICE_STATE_REASON_PPP_FAILED: PPP failed
	@NM_DEVICE_STATE_REASON_DHCP_START_FAILED: DHCP client failed to start
	@NM_DEVICE_STATE_REASON_DHCP_ERROR: DHCP client error
	@NM_DEVICE_STATE_REASON_DHCP_FAILED: DHCP client failed
	@NM_DEVICE_STATE_REASON_SHARED_START_FAILED: Shared connection service failed to start
	@NM_DEVICE_STATE_REASON_SHARED_FAILED: Shared connection service failed
	@NM_DEVICE_STATE_REASON_AUTOIP_START_FAILED: AutoIP service failed to start
	@NM_DEVICE_STATE_REASON_AUTOIP_ERROR: AutoIP service error
	@NM_DEVICE_STATE_REASON_AUTOIP_FAILED: AutoIP service failed
	@NM_DEVICE_STATE_REASON_MODEM_BUSY: The line is busy
	@NM_DEVICE_STATE_REASON_MODEM_NO_DIAL_TONE: No dial tone
	@NM_DEVICE_STATE_REASON_MODEM_NO_CARRIER: No carrier could be established
	@NM_DEVICE_STATE_REASON_MODEM_DIAL_TIMEOUT: The dialing request timed out
	@NM_DEVICE_STATE_REASON_MODEM_DIAL_FAILED: The dialing attempt failed
	@NM_DEVICE_STATE_REASON_MODEM_INIT_FAILED: Modem initialization failed
	@NM_DEVICE_STATE_REASON_GSM_APN_FAILED: Failed to select the specified APN
	@NM_DEVICE_STATE_REASON_GSM_REGISTRATION_NOT_SEARCHING: Not searching for networks
	@NM_DEVICE_STATE_REASON_GSM_REGISTRATION_DENIED: Network registration denied
	@NM_DEVICE_STATE_REASON_GSM_REGISTRATION_TIMEOUT: Network registration timed out
	@NM_DEVICE_STATE_REASON_GSM_REGISTRATION_FAILED: Failed to register with the requested network
	@NM_DEVICE_STATE_REASON_GSM_PIN_CHECK_FAILED: PIN check failed
	@NM_DEVICE_STATE_REASON_FIRMWARE_MISSING: Necessary firmware for the device may be missing
	@NM_DEVICE_STATE_REASON_REMOVED: The device was removed
	@NM_DEVICE_STATE_REASON_SLEEPING: NetworkManager went to sleep
	@NM_DEVICE_STATE_REASON_CONNECTION_REMOVED: The device's active connection disappeared
	@NM_DEVICE_STATE_REASON_USER_REQUESTED: Device disconnected by user or client
	@NM_DEVICE_STATE_REASON_CARRIER: Carrier/link changed
	@NM_DEVICE_STATE_REASON_CONNECTION_ASSUMED: The device's existing connection was assumed
	@NM_DEVICE_STATE_REASON_SUPPLICANT_AVAILABLE: The supplicant is now available
	@NM_DEVICE_STATE_REASON_MODEM_NOT_FOUND: The modem could not be found
	@NM_DEVICE_STATE_REASON_BT_FAILED: The Bluetooth connection failed or timed out
	@NM_DEVICE_STATE_REASON_GSM_SIM_NOT_INSERTED: GSM Modem's SIM Card not inserted
	@NM_DEVICE_STATE_REASON_GSM_SIM_PIN_REQUIRED: GSM Modem's SIM Pin required
	@NM_DEVICE_STATE_REASON_GSM_SIM_PUK_REQUIRED: GSM Modem's SIM Puk required
	@NM_DEVICE_STATE_REASON_GSM_SIM_WRONG: GSM Modem's SIM wrong
	@NM_DEVICE_STATE_REASON_LAST: Unused
    """
    NM_DEVICE_STATE_REASON_NONE = 0
    NM_DEVICE_STATE_REASON_UNKNOWN = 1
    NM_DEVICE_STATE_REASON_NOW_MANAGED = 2
    NM_DEVICE_STATE_REASON_NOW_UNMANAGED = 3
    NM_DEVICE_STATE_REASON_CONFIG_FAILED = 4
    NM_DEVICE_STATE_REASON_IP_CONFIG_UNAVAILABLE = 5
    NM_DEVICE_STATE_REASON_IP_CONFIG_EXPIRED = 6
    NM_DEVICE_STATE_REASON_NO_SECRETS = 7
    NM_DEVICE_STATE_REASON_SUPPLICANT_DISCONNECT = 8
    NM_DEVICE_STATE_REASON_SUPPLICANT_CONFIG_FAILED = 9
    NM_DEVICE_STATE_REASON_SUPPLICANT_FAILED = 10
    NM_DEVICE_STATE_REASON_SUPPLICANT_TIMEOUT = 11
    NM_DEVICE_STATE_REASON_PPP_START_FAILED = 12
    NM_DEVICE_STATE_REASON_PPP_DISCONNECT = 13
    NM_DEVICE_STATE_REASON_PPP_FAILED = 14
    NM_DEVICE_STATE_REASON_DHCP_START_FAILED = 15
    NM_DEVICE_STATE_REASON_DHCP_ERROR = 16
    NM_DEVICE_STATE_REASON_DHCP_FAILED = 17
    NM_DEVICE_STATE_REASON_SHARED_START_FAILED = 18
    NM_DEVICE_STATE_REASON_SHARED_FAILED = 19
    NM_DEVICE_STATE_REASON_AUTOIP_START_FAILED = 20
    NM_DEVICE_STATE_REASON_AUTOIP_ERROR = 21
    NM_DEVICE_STATE_REASON_AUTOIP_FAILED = 22
    NM_DEVICE_STATE_REASON_MODEM_BUSY = 23
    NM_DEVICE_STATE_REASON_MODEM_NO_DIAL_TONE = 24
    NM_DEVICE_STATE_REASON_MODEM_NO_CARRIER = 25
    NM_DEVICE_STATE_REASON_MODEM_DIAL_TIMEOUT = 26
    NM_DEVICE_STATE_REASON_MODEM_DIAL_FAILED = 27
    NM_DEVICE_STATE_REASON_MODEM_INIT_FAILED = 28
    NM_DEVICE_STATE_REASON_GSM_APN_FAILED = 29
    NM_DEVICE_STATE_REASON_GSM_REGISTRATION_NOT_SEARCHING = 30
    NM_DEVICE_STATE_REASON_GSM_REGISTRATION_DENIED = 31
    NM_DEVICE_STATE_REASON_GSM_REGISTRATION_TIMEOUT = 32
    NM_DEVICE_STATE_REASON_GSM_REGISTRATION_FAILED = 33
    NM_DEVICE_STATE_REASON_GSM_PIN_CHECK_FAILED = 34
    NM_DEVICE_STATE_REASON_FIRMWARE_MISSING = 35
    NM_DEVICE_STATE_REASON_REMOVED = 36
    NM_DEVICE_STATE_REASON_SLEEPING = 37
    NM_DEVICE_STATE_REASON_CONNECTION_REMOVED = 38
    NM_DEVICE_STATE_REASON_USER_REQUESTED = 39
    NM_DEVICE_STATE_REASON_CARRIER = 40
    NM_DEVICE_STATE_REASON_CONNECTION_ASSUMED = 41
    NM_DEVICE_STATE_REASON_SUPPLICANT_AVAILABLE = 42
    NM_DEVICE_STATE_REASON_MODEM_NOT_FOUND = 43
    NM_DEVICE_STATE_REASON_BT_FAILED = 44
    NM_DEVICE_STATE_REASON_GSM_SIM_NOT_INSERTED = 45
    NM_DEVICE_STATE_REASON_GSM_SIM_PIN_REQUIRED = 46
    NM_DEVICE_STATE_REASON_GSM_SIM_PUK_REQUIRED = 47
    NM_DEVICE_STATE_REASON_GSM_SIM_WRONG = 48
    NM_DEVICE_STATE_REASON_LAST = 0xFFFF


class NMActiveConnectionState(object):
    """

    """


__all__ = [
    "NMState",
    "NMDeviceType",
    "NMDeviceCapabilities",
    "NMDeviceWifiCapabilities",
    "NM80211ApFlags",
    "NM80211ApSecurityFlags",
    "NM80211Mode",
    "NMBluetoothCapabilities",
    "NMDeviceModemCapabilities",
    "NMDeviceState",
    "NMDeviceStateReason",
    "NMActiveConnectionState",
]
