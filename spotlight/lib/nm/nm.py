class DeviceManager(object):
    """
    Network manager does not implement the 'org.freedesktop.DBus.ObjectManager'
    for the Device objects it manages.

    This class adapts the 'org.freedesktop.NetworkManager' interface to be
    compatible with the {RemoteObjectManager} interface

    The the objects managed by this class are
    'org.freedesktop.NetworkManager.Device'
    """
    def __init__(self, dbusObject):
        self.dbusObject = dbusObject


    def connect(self):
        self.dbusObject.notifyOnSignal(
            'DeviceAdded',
            self.onDeviceAdded,
            interface='org.freedesktop.NetworkManager'
        )
        self.dbusObject.notifyOnSignal(
            'DeviceRemoved',
            self.onDeviceRemoved,
            interface='org.freedesktop.NetworkManager'
        )

    def onDeviceAdded(self, objectPath):
        """
        Callback for the 'DeviceAdded' signal. Adapts the signal to be
        compatible with 'InterfacesAdded' signal. Signal. heh

        This primarily means listing the objects interfaces and properties before
        """

    def onDeviceRemoved(self, objectPath):
        pass
