
class Device(object):
    """
    Signals:
    StateChanged (u new_state, du old_state, u reason);

    Properties:
    Udi                   readable   s
    Interface             readable   s
    IpInterface           readable   s
    Driver                readable   s
    DriverVersion         readable   s
    FirmwareVersion       readable   s
    Capabilities          readable   u
    Ip4Address            readable   u
    State                 readable   u
    StateReason           readable   (uu)
    ActiveConnection      readable   o
    Ip4Config             readable   o
    Dhcp4Config           readable   o
    Ip6Config             readable   o
    Dhcp6Config           readable   o
    Managed               readwrite  b
    Autoconnect           readwrite  b
    FirmwareMissing       readable   b
    NmPluginMissing       readable   b
    DeviceType            readable   u
    AvailableConnections  readable   ao
    PhysicalPortId        readable   s
    Mtu                   readable   u
    Metered               readable   u
    LldpNeighbors         readable   aa{sv}
    Real                  readable   b
    """
