Spotlight
=========
Spotlight is a toolkit containing libraries, services, scripts, and various
other tools that are intended to simplify and unify the creation of

A partial list of tools includes:
 * vnStat: "ctypes" based database parser. Eventually high level tools.
 * downtimed: "ctypes" based database parser. Eventually high level tools.
 * NetworkManager: DBus wrapper for listing network interfaces and state changes
 * ModemManager: DBus wrapper for listing modems properties and state changes
 * LCD Userspace driver: High-level driver sitting atop the low-level kernel driver
 * EEPROM: Parsers for BeagleBone cape and board EEPROM identity stores.
 * Bootloader: Inspectors and extractors for U-Boot am335x based bootloaders
 * Hardware: Encapsulates the enumeration of important system hardware.

Todo:
 * Keystore: Uniform and secure storage of PKI credentials
 * Firstboot: Lightweight framework for doing stateful things on firstboot
